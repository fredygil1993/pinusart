<?php

//Route::get('/', function () {
  //  return view('index');
//})->middleware('guest')->middleware('disablepreventback');

Route::get('/', 'IndexController@index')->middleware('guest')->middleware('disablepreventback');

Auth::routes(['verify' => true]);

Route::get('/pruebaNav', function () {
    return view('usuarioAdmin.Productos.index');
})->middleware('disablepreventback');

Route::get('/cliente', function () {
    return view('usuarioStandar.Cliente.create');
})->middleware('disablepreventback');

// aca cosas qye solo puede ver el administrador
Route::group(['middleware' => 'usuarioAdmin'], function () {
    Route::get('/home','homeController@index')->name('home')->middleware('verified')->middleware('auth')->middleware('disablepreventback');

    Route::resource('Productos','ProductsController')->middleware('auth');
});

// aca termina las vistas solo para e administrador

// aca vistas que solo puede ver el usuario normal

Route::group(['middleware' => 'usuarioStandard'], function () {

    Route::get('/usuario','homeController@standar')->name('usuarioStandar')->middleware('verified')->middleware('auth')->middleware('disablepreventback');

    Route::resource('Cliente','ClientController')->middleware('auth');

});
// aca termina las vistas que puede ver el usuario normal
Route::resource('Perfil','perfilcontroller')->middleware('auth');

Route::get('/CatalogoProductos',[
    'as'=> 'home',
    'uses' => 'CatalogoUserController@index'
])->middleware('auth');

Route::get('/CatalogoProductos/{id}',[
    'as'=> 'product-detail',
    'uses' => 'CatalogoUserController@show'
])->middleware('auth');

Route::resource('CategoriaProductos','ProductCategoryController')->middleware('auth');

Route::resource('usuarioAdmin','PurposesController')->middleware('auth');

Route::resource('ProductoFinal','ProductFinalController')->middleware('auth');

Route::resource('FichaTecnica','DataController')->middleware('auth');

Route::resource('Pedido','statuscontroller')->middleware('auth');

Route::resource('InformeClientes','informeController')->middleware('auth');

Route::resource('TipoInsumos','typesuppliecontroller')->middleware('auth');

Route::resource('Insumos','suppliecontroller')->middleware('auth');

Route::resource('Tarifas','ratecontroller')->middleware('auth');

Route::resource('Pedido','statuscontroller')->middleware('auth');

Route::resource('Pedidos','pedidocontroller')->middleware('auth');

Route::resource('Informes','detalleController')->middleware('auth');

Route::resource('Galeria','galerycontroller');



