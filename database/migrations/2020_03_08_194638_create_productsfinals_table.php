<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsfinalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('productsfinals', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('iddatasheet')->nullable()->unsigned();
            $table->Integer('idproduct')->unsigned();
            $table->bigInteger('idrate')->unsigned();
            $table->text('description')->nullable();
            $table->foreign('idrate')->references('id')->on('annualrates');
            $table->foreign('idproduct')->references('id')->on('products');
            $table->foreign('iddatasheet')->references('id')->on('datasheets');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('productsfinals');
    }
}
