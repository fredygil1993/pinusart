<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDatasheetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('datasheets', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('idclient')->unsigned();
            $table->bigInteger('idstate')->unsigned();
            $table->string('date');
            $table->integer('quantity')->nullable();
            $table->float('total')->nullable();
            $table->foreign('idclient')->references('id')->on('clients');
            $table->foreign('idstate')->references('id')->on('states');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('datasheets');
    }
}
