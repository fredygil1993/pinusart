<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnnualratesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('annualrates', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('idsupplie')->unsigned();
            $table->integer('year');
            $table->float('value');
            $table->foreign('idsupplie')->references('id')->on('supplies');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('annualrates');
    }
}
