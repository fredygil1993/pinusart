<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('photo');
            $table->bigInteger('type_product')->unsigned();
            $table->string('name_product');
            $table->string('size');
            $table->double('high');
            $table->double('depth');
            $table->double('width');
            $table->integer('year');
            $table->float('value_unit');
            $table->float('value_max12');
            $table->string('description');
            $table->boolean('state');
            $table->foreign('type_product')->references('id')->on('category_products');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
