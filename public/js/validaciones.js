function soloNumero(evt) {
    evt = (evt) ? evt : window.event
    var charCode = (evt.which) ? evt.which : evt.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        evt.preventDefault();
        status = "This field accepts numbers only."
        return false
    }
    return true
};

function ValCategoriaProducto(id) {
    var categoria;
    categoria = $('#categoria').val();
    if (categoria===""){
        $('#btnCategoria').attr("disabled", true);
        if(categoria===""){
            $('#categoriaErr').attr("hidden",false);
        }else  {
            $('#categoriaErr').attr("hidden",true);
        }
    }else {
        $('#btnCategoria').attr("disabled", false);
    }

};
function añoselect(id) {
    var min = 2020,
        max = 2050,
        select = document.getElementById('año');
    for (var i = min; i<=max; i++){
        var opt = document.createElement('option');
        opt.value = i;
        opt.innerHTML = i;
        select.appendChild(opt);
    }
    select.value = new Date().getFullYear();

}



function ValidarUsuario() {
    var avatar, name, lastname, phone, address;
    avatar = $('#avatar').val();
    name = $('#name').val();
    lastname = $('#lastname').val();
    phone = $('#phone').val();
    address = $('#address').val();
    if (avatar ==="" || name==="" || lastname ==="" || phone ==="" || address ===""){

        $('#botonEnviar').attr("disabled", true);
        if(avatar===""){
            $('#avatarErr').attr("hidden",false);
        }else  {
            $('#avatarErr').attr("hidden",true);
        }

        if (name===""){
            $('#nameErr').attr("hidden",false);
        }else {
            $('#namnameErreErr').attr("hidden",true);
        }

        if (lastname===""){
            $('#lastnameErr').attr("hidden",false);
        }else {
            $('#lastnameErr').attr("hidden",true);
        }

        if (phone===""){
            $('#phoneErr').attr("hidden",false);
        }else {
            $('#phoneErr').attr("hidden",true);
        }

        if (address===""){
            $('#addressErr').attr("hidden",false);
        }else {
            $('#phoneErr').attr("hidden",true);
        }

        return false;
    }else {
        $('#botonEnviar').attr("disabled", false);
    }
}
function valProductoNuevo() {
    var nombre, categoria, tamaño, alto, Ancho, precio,  precio2, imagen, descripcion,año,profundidad;
    nombre = $('#nombre').val();
    categoria = $('#categoria').val();
    tamaño = $('#tamaño').val();
    alto = $('#alto').val();
    ancho = $('#ancho').val();
    profundidad = $('#profundidad').val();
    precio = $('#precio').val();
    precio2 = $('#precio2').val();
    imagen = $('#imagen').val();
    descripcion = $('#descripcion').val();
    año = $('#año').val();
    if (nombre==="" || categoria==="" || tamaño==="" || alto==="" || ancho==="" || año==="" || precio==="" || precio2==="" || imagen==="" || descripcion==="" || profundidad===""){

        $('#buttonRegistrar').attr("disabled", true);
        if(nombre===""){
            $('#nombreErr').attr("hidden",false);
        }else  {
            $('#nombreErr').attr("hidden",true);
        }
        if (categoria===""){
            $('#categoriaErr').attr("hidden",false);
        }else {
            $('#categoriaErr').attr("hidden",true);
        }
        if (tamaño===""){
            $('#tamañoErr').attr("hidden",false);
        }else {
            $('#tamañoErr').attr("hidden",true);
        }
        if (alto===""){
            $('#altoErr').attr("hidden",false);
        }else {
            $('#altoErr').attr("hidden",true);
        }
        if (ancho===""){
            $('#anchoErr').attr("hidden",false);
        }else {
            $('#anchoErr').attr("hidden",true);
        }
        if (año===""){
            $('#añoErr').attr("hidden",false);
        }else {
            $('#añoErr').attr("hidden",true);
        }
        if (precio===""){
            $('#precioErr').attr("hidden",false);
        }else {
            $('#precioErr').attr("hidden",true);
        }
        if (precio2===""){
            $('#precio2Err').attr("hidden",false);
        }else {
            $('#precio2Err').attr("hidden",true);
        }
        if (imagen===""){
            $('#imagenErr').attr("hidden",false);
        }else {
            $('#imagenErr').attr("hidden",true);
        }
        if (descripcion===""){
            $('#descripcionErr').attr("hidden",false);
        }else {
            $('#descripcionErr').attr("hidden",true);
        }if (profundidad===""){
            $('#profundidadErr').attr("hidden",false);
        }else {
            $('#profundidadErr').attr("hidden",true);
        }
    }else {
        $('#buttonRegistrar').attr("disabled", false);
    }
}
function registroExitoso() {
    Swal.fire({
        icon: 'success',
        title: 'Registro exitoso',
        showConfirmButton: false,
        timer: 1550
    })
}
function valCategoria() {
    var categoria;
    categoria = $('#categoria').val();
    if (categoria===""){
        $('#btnCrearCatego').attr("disabled", true);
        $('#categoriaErr').attr("hidden",false);
    }else {
        $('#btnCrearCatego').attr("disabled", false);
        $('#categoriaErr').attr("hidden",true);
    }
}
function valproposito() {
    var mision, vision, quehacemos;
    mision = $('#mision').val();
    vision = $('#vision').val();
    quehacemos = $('#quehacemos').val();
    if (mision==="" || vision==="" || quehacemos===""){
        $('#buttonRegistrar').attr("disabled",true);
        $('#misionErr').attr("hidden",false);
        $('#visionErr').attr("hidden",false);
        $('#quehacemosErr').attr("hidden",false);
    }else {
        $('#buttonRegistrar').attr("disabled",false);
        $('#misionErr').attr("hidden",true);
        $('#visionErr').attr("hidden",true);
        $('#quehacemosErr').attr("hidden",true);
    }
}

function cambiosExitosos() {
    Swal.fire({
        icon: 'success',
        title: 'Se a cambiado correctamente',
        showConfirmButton: false,
        timer: 2000
    })
}
function ValidarTipoinsumo() {
    var descripcion;
    descripcion = $('#descripcion').val();
    if (descripcion===""){
        $('#btnCreartipo').attr("disabled",true);
        $('#descripcionErr').attr("hidden",false);
    }else {
        $('#btnCreartipo').attr("disabled",false);
        $('#descripcionErr').attr("hidden",true);
    }
}function Validarinsumo() {
    var nombre, idtipo;
    idtipo = $('#idtipo').val();
    nombre = $('#nombre').val();
    if (idtipo==="" || nombre===""){
        $('#btnCrearInsumo').attr("disabled",true);
        $('#nombreErr').attr("hidden",false);
        $('#idtipoErr').attr("hidden",false);
    }else {
        $('#btnCrearInsumo').attr("disabled",false);
        $('#nombreErr').attr("hidden",true);
        $('#idtipoErr').attr("hidden",true);
    }
}
function ValidarTarifa() {
    var idinsumos, año, valor;
    idinsumos = $('#idinsumos').val();
    año = $('#año').val();
    valor = $('#valor').val();
    if (idinsumos==="" || año==="" || valor===""){
        $('#btnCrearTarifa').attr("disabled",true);
        $('#idinsumosErr').attr("hidden",false);
        $('#añoErr').attr("hidden",false);
        $('#valorErr').attr("hidden",false);
    }else {
        $('#btnCrearTarifa').attr("disabled",false);
        $('#idinsumosErr').attr("hidden",true);
        $('#añoErr').attr("hidden",true);
        $('#valorErr').attr("hidden",true);
    }
}
function ValidarEditPerfil() {
    var name, lastname, phone, address;
    $('#name').attr("readonly",false);
    $('#lastname').attr("readonly",false);
    $('#phone').attr("readonly",false);
    $('#address').attr("readonly",false);

        $('#btnCrearCliente').attr("hidden",false);
}

$('#limpiarCampos').click(function (e) {
    e.preventDefault();
    swal.fire({
        title: '¿Estás seguro?',
        text: "Se borrará la información y no se puedra recuperar!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Sí, limpiar los campos!'
    }).then((result) => {
    if (result.value) {
        Swal.fire(
            '¡Campos vacíos!',
            'Todos los campos fueron limpiados',
            'success'
        )
        $('#idproductoT').val("");
        $('#idproducto').val("");
        $('#idtarifaT').val("");
        $('#idtarifa').val("");
        $('#idrate2T').val("");
        $('#idrate2').val("");
        $('#descripcion').val("");
        $('#cantidad').val("");
        $('#total').val("");
        $("#lista_agregar").remove();
        $('#idtarifaT').prop("disabled",false);
        $('#idrate2T').prop("disabled",false)
    }
})
});
function productofinal() {
    var idproductoT, idtarifaT, idrate2T,cantidad;
    idproductoT = $('#idproductoT').val();
    idtarifaT = $('#idtarifaT').val();
    idrate2T = $('#idrate2T').val();
    cantidad = $('#cantidad').val();

    if (idproductoT==="" || idtarifaT==="" || idrate2T==="" || cantidad===""){
        $('#calcular').attr("disabled",true);
        if (idproductoT===""){
            $('#idproductoTErr').attr("hidden",false);
        }else {
            $('#idproductoTErr').attr("hidden",true);
        }
        if (idtarifaT===""){
            $('#idtarifaTErr').attr("hidden",false);
        }else {
            $('#idtarifaTErr').attr("hidden",true);
        }
        if (idrate2T===""){
            $('#idrate2TErr').attr("hidden",false);
        }else {
            $('#idrate2TErr').attr("hidden",true);
        }
        if (cantidad===""){
            $('#cantidadErr').attr("hidden",false);
        }else {
            $('#cantidadErr').attr("hidden",true);
        }
    }else{
        $('#calcular').attr("disabled",false);
    }
}

