@extends('layouts.nav')
@section('name')
    {{-- esto es para mostrar la imagen y para mostrar el nombre asociado --}}
    <span class="mr-2 d-none d-lg-inline text-white small">
	@foreach ($clientes as $cliente)

            @if(auth()->check() && $cliente->id_user == $usuario->id)
                <img class="" height="70" src="Avatares/{{$cliente->avatar}}" style="width: 25px; height: 25px" >
                {{ $cliente->name}}
            @endif
        @endforeach
    </span>
    {{-- termina el mostrar --}}
@endsection
@section('contenidoNav')
    <div class="container mt-5">
        <table class="table prueba" >
            <thead>
            <tr>
                <h3 class="text-white" style="margin-left: 35%"> Listado de Pedidos</h3>
            </tr>
            <tr>
                <th scope="col" class="text-white">ID</th>
                <th scope="col" class="text-white" >Fecha</th>
                <th scope="col"class="text-white">Nombre producto</th>
                <th scope="col"class="text-white">Cantidad</th>
                <th scope="col"class="text-white">Total</th>
                <th scope="col"class="text-white">Estados</th>
            </tr>
            </thead>
            <tbody>
 resources/views/usuarioStandar/Pedido/index.blade.php
                @foreach($ficha as $fichas)
                    <tr>
                        <td class="text-white">{{$fichas->date}}</td>
                        <td class="text-white">{{$fichas->name_product}}</td>
                        <td class="text-white">{{$fichas->quantity}}</td>
                        <td class="text-white">{{$fichas->total}}</td>
                        <td class="text-white">{{$fichas->state}}</td>
                    </tr>
                @endforeach

            @foreach($ficha as $fichas)
                <tr>
                    <td class="text-white">{{$fichas->id}}</td>
                    <td class="text-white">{{$fichas->date}}</td>
                    <td class="text-white">{{$fichas->name_product}}</td>
                    <td class="text-white">{{$fichas->quantity}}</td>
                    <td class="text-white">{{$fichas->total}}</td>
                    <td class="text-white">{{$fichas->state}}</td>
                    <td style="color: #0a0a0a"><a  class="btn btn-warning" href="/ProductoFinal/{{$fichas->id}}/edit" ><span class="icon-pencil2"></span></a>
                    </td>
                    {{--href="{{URL::action('statuscontroller@edit',$fichas->id)}}"--}}
                </tr>
            @endforeach
resources/views/usuarioStandar/Pedido/index.blade.php
            </tbody>
        </table>
    </div>
@endsection
