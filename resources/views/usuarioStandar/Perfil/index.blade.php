@extends('layouts.nav')
@section('name')
    {{-- termina el mostrar --}}
@endsection
@section('contenidoNav')
    <div class="container">
        <div class="card col-md-10 prueba " style=" margin-left: 7.5%">
            <div class="card-header">
                <h6 style="margin-left: 39%" class="text-black-50">Perfil</h6>
            </div>
            <div class="card-body">
                <div class="container">
                    <form class="" method="POST" action="{{route('Perfil.update',$usuario->id)}}" enctype="multipart/form-data" id="formulario">
                        {{ csrf_field() }}
                        @method('PUT')
                        @foreach ($cliente as $clientes)
                            <div class="form-group row">
                                <div class="col-sm-4">
                                    <img class="avatar" src="Avatares/{{$clientes->avatar}}" width="180" height="190" style="border-radius: 20px 30px 10px">
                                    <ul style="margin-left: 8%"><a href="">cambiar</a></ul>
                                </div>
                                <div class="col-sm-8">
                                    <label for="email" class=" control-label">Correo Electronico</label>
                                    <input id="email" type="text" class="form-control" name="email" autofocus readonly value="{{$clientes->email}}">
                                    <br>
                                    <div class="form-group row">
                                        <div class="col-6">
                                            <label for="name" class="col-md-6 control-label">Nombres</label>
                                            <input id="name" type="text" class="form-control" name="name"  value="{{$clientes->name}}"  readonly>
                                            <h6 hidden id="nameErr" class="text-danger">Nombre no valido</h6></div>
                                        <div class="col-6">
                                            <label for="name" class="col-md-6 control-label">Apellidos</label>
                                            <input id="lastname" type="text" class="form-control" name="lastname" required autofocus value="{{$clientes->lastname}}" readonly>                                            <h6 hidden id="nameErr" class="text-danger">Nombre no valido</h6>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="form-group row">
                                        <div class="col-6">
                                            <label for="name" class="col-md-8 control-label">Numero telefonico</label>
                                            <input id="phone" type="text" class="form-control" name="phone"  required autofocus onclick="soloNumero()" onkeyup="soloNumero()" onkeypress="soloNumero()" onkeydown="soloNumero()" value="{{$clientes->phone}}" readonly>
                                            <h6 hidden id="nameErr" class="text-danger">Numero no valido</h6></div>
                                        <div class="col-6">
                                            <label for="address" class="col-md-6 control-label">Direccion</label>
                                            <input id="address" type="text" class="form-control" name="address"  required autofocus value="{{$clientes->address}}" readonly>
                                            <h6 hidden id="addressErr" class="text-danger"> Direccion no valida</h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-4">

                                </div>
                                <div class="col-8">
                                    <button type="submit" id="btnenviar" hidden></button>
                                </div>
                            </div>
                        @endforeach
                    </form>
                    <div class="row">
                        <div class="col-4"></div>
                        <div class="col-8">
                            <button hidden id="btnCrearCliente" class="btn btn-success" onclick="editar()">Guardar</button>
                            <button class="btn btn-success" id="Edit" onclick="ValidarEditPerfil()">Editar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection



