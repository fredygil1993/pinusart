<form class="" method="POST" action="{{route('Perfil.update',$clientes->id_user)}}" enctype="multipart/form-data">
    {{ csrf_field() }}
    @method('PUT')
    <div class="form-row">
        <div class="form-group col-md-6">
            <label for="name" class="col-md-6 control-label">nombre</label>
            <input id="name" type="text" class="form-control" name="name"  value="{{$clientes->name}}">
            <h6 hidden id="nameErr" class="text-danger">Nombre no valido</h6>
        </div>
        <div class="form-group col-md-6">
            <label for="lastname" class="col-md-6 control-label">Apellido</label>
            <input id="lastname" type="text" class="form-control" name="lastname" required autofocus value="{{$clientes->lastname}}">
            <h6 hidden id="lastnameErr" class="text-danger">Apellido no valido</h6>
        </div>
        <div class="form-group col-md-6">
            <label for="number" class="col-md-8 control-label">Numero telefonico</label>
            <input id="phone" type="text" class="form-control" name="phone"  required autofocus onclick="soloNumero()" onkeyup="soloNumero()" onkeypress="soloNumero()" onkeydown="soloNumero()" value="{{$clientes->phone}}">
            <h6 hidden id="phoneErr" class="text-danger">Numero no valido</h6>
        </div>
        <div class="form-group col-md-6">
            <label for="address" class="col-md-6 control-label">Direccion</label>
            <input id="address" type="text" class="form-control" name="address"  required autofocus value="{{$clientes->address}}">
            <h6 hidden id="addressErr" class="text-danger"> Direccion no valida</h6>
        </div>
    </div>
    <div class="col-sm-4" onmouseover="ValidarUsuario()">
        <button id="btnCrearCliente" class="btn btn-success" type="submit" onclick="registroExitoso()">Editar</button>
    </div>
</form>
