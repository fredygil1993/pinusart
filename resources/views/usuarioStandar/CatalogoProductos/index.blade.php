@extends('layouts.nav')
@section('buscar')
    <form class="d-none d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search">
        <div class="input-group">
            <input type="text" class="form-control bg-light border-0 small" placeholder="Buscar" aria-label="Search" aria-describedby="basic-addon2">
            <div class="input-group-append">
                <button class="btn btn-primary" type="button">
                    <span class="icon-search"></span>
                </button>
            </div>
        </div>
    </form>
@endsection
@section('name')
    {{-- esto es para mostrar la imagen y para mostrar el nombre asociado --}}
    <span class="mr-2 d-none d-lg-inline text-white small">
	@foreach ($clientes as $cliente)
            @if(auth()->check() && $cliente->id_user == $usuario->id)
                <img  height="70" src="Avatares/{{$cliente->avatar}}" style="width: 25px; height: 25px" >
                {{ $cliente->name}}
            @endif
        @endforeach
</span>
    {{-- termina el mostrar --}}
@endsection
@section('contenidoNav')
    <div class="container text-center" >
        <div id="products">
            @foreach($Listar as $list)
                <div class="product white-panel">
                    <h1 style="color: #0a0a0a">{{$list->name_product}}</h1>
                    <img src="GaleriaProductos/{{$list->photo}}"  >
                     <div class=" mt-2 product-info panel">
                        <p style="color: #0a0a0a">Precio: ${{ number_format($list->value_unit,2)}}</p>
                        <p> <a class="btn btn-warning" href="/ProductoFinal"><span style="color: #0a0a0a" class="icon-cart"></span></a>
                            <a class="btn btn-primary" href="{{route('product-detail', $list->id)}}"><span  class="icon-circle-right"></span> Leer Mas</a></p>
                     </div>
                </div>
            @endforeach
        </div>
    </div>

@endsection
@section('script')
    <style>
        #products {
            position: relative;
            max-width: 100%;
            width: 100%;
        }
        img {
            width: 100%;
            max-width: 100%;
            height: auto;
        }
        .white-panel {
            position: absolute;
            background: white;
            box-shadow: 0px 1px 2px rgba(0,0,0,0.3);
            padding: 10px;
        }
        .white-panel h1 {
            font-size: 1em;
        }
        .white-panel h1 a {
            color: #A92733;
        }
        .white-panel:hover {
            box-shadow: 1px 1px 10px rgba(0,0,0,0.5);
            margin-top: -5px;
            -webkit-transition: all 0.3s ease-in-out;
            -moz-transition: all 0.3s ease-in-out;
            -o-transition: all 0.3s ease-in-out;
            transition: all 0.3s ease-in-out;
        }
    </style>
    <script>
        $(document).ready(function() {
            $('#products').pinterest_grid({
                no_columns: 3,
                padding_x: 15,
                padding_y: 15,
                margin_bottom: 50,
                single_column_breakpoint: 800
            });
        });
    </script>

@endsection
