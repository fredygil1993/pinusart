@extends('layouts.nav')
@section('name')
    {{-- esto es para mostrar la imagen y para mostrar el nombre asociado --}}
    <span class="mr-2 d-none d-lg-inline text-white small">
	@foreach ($clientes as $cliente)
            @if(auth()->check() && $cliente->id_user == $usuario->id)
                <img class="" height="70" src="/Avatares/{{$cliente->avatar}}" style="width: 25px; height: 25px" >
                {{ $cliente->name}}
            @endif
        @endforeach
</span>
    {{-- termina el mostrar --}}
@endsection
@section('contenidoNav')
    <div class="container text-center">
        <div class="page-header">
            <h3 style="color: white; margin-bottom: 2%">Detalle del Producto</h3>
        </div>
        <div class="row">
            <div class="col-md-6" >
                <div class="card bloque-producto">
                    <img src="/GaleriaProductos/{{$product->photo}}" width="350">
                </div>
            </div>
            <div class="col-md-6">
                <div class="card card-body container detalle-producto">
                    <h4 style="color: #0a0a0a">{{$product->name_product}}</h4>
                    <div class="container info" style="font-size: 1em; color: #0a0a0a">
                        <p >Categoria: {{$product->type_product}}</p>
                        <p>Tamaño: {{$product->size}}</p>
                        <p>Alto: {{$product->high}} cm</p>
                        <p>Ancho: {{$product->width}} cm</p>
                        <p>Profundidad: {{$product->depth}} cm</p>
                        <p>Precio: ${{ number_format($product->value_unit,2)}}</p>
                        <p>Precio: ${{ number_format($product->value_max12,2)}}</p>
                        <p>{{$product->description}}</p>
                        <p>
                            <a class="btn btn-block btn-warning" href="/ProductoFinal"><span style="color: #0a0a0a" class="icon-cart"> La quiero</a>
                        </p>
                    </div>
                    <a class="btn btn-block text-white" href="javascript:history.back()" style="background-color: #0a0a0a"><span class="icon-undo2"></span></a>
                </div>
            </div>
        </div>
    </div>
@endsection
