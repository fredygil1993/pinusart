@extends('layouts.nav')
@section('name')
    {{-- esto es para mostrar la imagen y para mostrar el nombre asociado --}}
    <span class="mr-2 d-none d-lg-inline text-white small">
	@foreach ($clientes as $cliente)
            @if(auth()->check() && $cliente->id_user == $usuario->id)
                <img class="" height="70" src="Avatares/{{$cliente->avatar}}" style="width: 25px; height: 25px" >
                {{ $cliente->name}}
            @endif
        @endforeach
    </span>
    {{-- termina el mostrar --}}
@endsection
@section('contenidoNav')
    <div class="container">
        <div class="card col-md-10 prueba " style=" margin-left: 4.5%">
            <div class="card-header">
                <h6 style="margin-left: 39%" class="text-black-50">Producto final</h6>
            </div>
            <div class="card-body">
                <div class="container">
                    <form id="Form" class="" method="POST" action="{{route('ProductoFinal.store')}}">
                        {{ csrf_field() }}
                        <div class="form-row">
                        </div>
                        <div class="form-row col">
                            <div class=" row col-md-12" style="margin-left: 6%" >
                                <div class="form-group col-md-4">
                                    <label for="">Producto</label>
                                    <select class="form-control"  id="idproductoT" >
                                        <option value="">Seleccionar</option>
                                        @foreach($productos as $product)
                                            <option value="{{$product->id}}:{{$product->value_unit}}" >{{$product->name_product}}</option>
                                        @endforeach
                                    </select>
                                    <h6 hidden id="idproductoTErr" style="margin-top: 2%" class="text-danger">Producto no valido</h6>
                                    <input type="text" hidden value="" id="idproducto" name="idproducto">
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="">Maderas</label>
                                    <select id="idtarifaT" class="form-control"  >
                                        <option value="">Seleccionar</option>
                                        @foreach($tarifa2 as $tar)
                                            <option  value="{{$tar->id}}:{{$tar->name}}:{{$tar->year}}:{{$tar->value}}">{{$tar->name}}</option>
                                        @endforeach
                                    </select>
                                    <h6 hidden id="idtarifaTErr" style="margin-top: 2%" class="text-danger">Madera No valida</h6>
                                    <input type="number" hidden value="" id="idtarifa" name="idtarifa">
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="">Cordones</label>
                                    <select id="idrate2T" class="form-control" >
                                        <option value="">Seleccionar</option>
                                        @foreach($tarifa as $tar)
                                            <option  value="{{$tar->id}}:{{$tar->name}}:{{$tar->year}}:{{$tar->value}}">{{$tar->name}}</option>
                                        @endforeach
                                    </select>
                                    <h6 hidden id="idrate2TErr" style="margin-top: 2%" class="text-danger">Cordon No valido</h6>
                                    <input type="number" hidden value="" id="idrate2" name="idrate2">
                                </div>
                                <div style="margin-top: 5%">
                                    <button class="btn btn-primary" id="btnAgregar" name="btnAgregar" type="button" style="margin-left: 2.5%; margin-top: -27%">+</button>
                                </div>
                            </div>
                            <div class="container col-md-9" style="margin-left: 10%">
                                <table  class="table  col-md-12 ">

                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Nombre</th>
                                        <th>Año</th>
                                        <th>Valor</th>
                                    </tr>
                                    </thead>
                                    <tbody id="lista_agregar">
                                    </tbody>
                                </table>
                            </div>
                            <div class="row col-md-10" style="margin-left: 16%">
                                <div class="form-group col-md-9">
                                    <label for="">Descripcion</label>
                                    <textarea  type="text"name="descripcion" id="descripcion" class="form-control" cols="5" rows="2"></textarea>
                                </div>
                                <div class="col-sm-2"style="margin-top: 9%">

                                </div>
                            </div>
                        </div>
                        <div class="form-row col-md-4" style="margin-left: 30%">
                            <input class="form-control" type="text" id="idcliente" name="idcliente" value="{{$usuario2}}" hidden>
                        </div>
                        <div class="form-row col-md-12" >
                            @foreach($estado as $est)
                                <input id="idestado" type="text" class="form-control  col-md-11" name="idestado"  style="margin-left: 2%" hidden value="{{$est->id}}" >
                            @endforeach
                        </div>
                        <div class="form-group col-md-12" style="">
                            <input  class="form-control  col-md-11" type="date" name="fechaActual" id="fechaActual" hidden value="" >
                        </div>
                        <div class="row col-md-10" style="margin-left: 16%">
                            <div class="form-group col-md-2"  >
                                <label for="">Cantidad</label>
                                <input type="text" class="form-control" id="cantidad" name="cantidad" minlength="1" maxlength="2" onkeypress="soloNumero()">
                                <h6 hidden id="cantidadErr" style="margin-top: 2%" class="text-danger">Cantidad no valida</h6>
                            </div>
                            <div class="form-group col-md-7" >
                                <label for="">Total</label>
                                <input type="text" class="form-control" id="total" name="total" readonly >
                            </div>
                        </div>
                        <div class=" row form-group" style="margin-left: 16%" onmousemove="productofinal()">
                            <button class="btn btn-warning" id="limpiarCampos" name="limpiarCampos" >Limpiar Campos</button>
                            <button class="btn text-white"  id="calcular" style="background-color: #0b2e13; margin-left:3%"> Calcular</button>
                            <!--a  class="btn text-white"  id="calcular" style="background-color: #0b2e13; margin-left:3%"> Calcular</a-->
                            <button disabled  class="btn text-white" id="enviar" type="submit" style="background-color: #0b2e13; margin-left:3%"> Crear</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <style>
        .prueba{
            -webkit-box-shadow: 1px 0px 15px 9px rgba(0,0,0,0.38);
            -moz-box-shadow: 1px 0px 15px 9px rgba(0,0,0,0.38);
            box-shadow: 1px 0px 15px 9px rgba(0,0,0,0.38);
        }
    </style>
@endsection
@section('script')
    <script>
        window.onload = function(){
            var fecha = new Date(); //Fecha actual
            var mes = fecha.getMonth()+1; //obteniendo mes
            var dia = fecha.getDate(); //obteniendo dia
            var ano = fecha.getFullYear(); //obteniendo año
            if(dia<10)
                dia='0'+dia; //agrega cero si el menor de 10
            if(mes<10)
                mes='0'+mes //agrega cero si el menor de 10
            document.getElementById('fechaActual').value=ano+"-"+mes+"-"+dia;
        }
        var contadorm=0;
        var contadort=0;
        var acumulador=0;
        var valores=0;

        $('#calcular').click(function () {
            var idproducto=$('#idproductoT').val()
            var array=idproducto.split(":");
            $('#idproducto').val(array[0])

            var valorproduc=array[1]

            console.log(array)

            if(valorproduc.length>0){
                if(acumulador<1){

                    console.log(valores)
                    valores=(parseInt(valores)+parseInt(valorproduc))*parseInt($('#cantidad').val())
                    acumulador++
                    $('#total').val(valores)
                    $('#enviar').prop("disabled",false)

                }
            }
        })

        $('#btnAgregar').click(function(e){
            e.preventDefault();
            var idrate2= $("#idrate2T").val();
            var idtarifa= $("#idtarifaT").val();

            if(idtarifa.length>0 && contadorm <1){
                var array=idtarifa.split(":")

                        $("#lista_agregar").append(''+
                            '<tr>' +
                            '<th>'+array[0]+'</th>' +
                            '<th>'+array[1]+'</th>' +
                            '<th>'+array[2]+'</th>' +
                            '<th>'+array[3]+'</th>' +
                            '</tr>');
                valores=parseInt(valores)+parseInt(array[3])
                $('#idtarifa').val(array[0])
                $('#idtarifaT').prop("disabled",true)
                console.log($('#idtarifa').val())
                contadorm++
                    }
             if(idrate2.length>0 && contadort<1 ){
                var array=idrate2.split(":")

                 $("#lista_agregar").append(''+
                     '<tr>' +
                     '<th>'+array[0]+'</th>' +
                     '<th>'+array[1]+'</th>' +
                     '<th>'+array[2]+'</th>' +
                     '<th>'+array[3]+'</th>' +
                     '</tr>');

                 valores=parseInt(valores)+parseInt(array[3])
                 $('#idrate2').val(array[0])
                 $('#idrate2T').prop("disabled",true)
                 contadort++
            }

        });
    </script>
@endsection
