@extends('layouts.nav')
@section('name')
    {{-- esto es para mostrar la imagen y para mostrar el nombre asociado --}}
    <span class="mr-2 d-none d-lg-inline text-white small">
	@foreach ($clientes as $cliente)
            @if(auth()->check() && $cliente->id_user == $usuario->id)
                <img class="" height="70" src="Avatares/{{$cliente->avatar}}" style="width: 25px; height: 25px" >
                {{ $cliente->name}}
            @endif
        @endforeach
</span>
    {{-- termina el mostrar --}}
@endsection
@section('contenidoNav')
    <div class="container mt-5">
        <div class="card col-md-6 prueba" style=" margin-left: 20%">
            <div class="card-header" >
               Solicitar Pedido
            </div>
            <div class="card-body">
                <div class="container" style="margin-left: 4%">
                    <form class="disabled" method="POST" action="{{route('FichaTecnica.store')}}">
                        {{ csrf_field() }}

                    </form>
                </div>
            </div>
        </div>
    </div>
    <style>
        .prueba{
            -webkit-box-shadow: 1px 0px 15px 9px rgba(0,0,0,0.38);
            -moz-box-shadow: 1px 0px 15px 9px rgba(0,0,0,0.38);
            box-shadow: 1px 0px 15px 9px rgba(0,0,0,0.38);
        }
    </style>
@endsection
@section('script')
    <script>

    </script>
@endsection
