@extends('layouts.nav')
@section('contenidoNav')
    <div id="wrapper">
        <div class="card col-md-6" style="margin-left: 25%">
            <div class="card-header">
                Cliente Nuevo
            </div>
            <div class="card-body">
                <form class="" method="POST" action="{{route('Cliente.store')}}" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="">Avatar </label>
                        <input type="file" name="avatar" id="avatar" >
                        <H6 hidden id="avatarErr" class="text-danger">Elija una imagen</H6>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="name" class="col-md-6 control-label">nombre</label>
                            <input id="name" type="text" class="form-control" name="name"  required autofocus>
                            <h6 hidden id="nameErr" class="text-danger">Nombre no valido</h6>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="lastname" class="col-md-6 control-label">Apellido</label>
                            <input id="lastname" type="text" class="form-control" name="lastname"  required autofocus>
                            <h6 hidden id="lastnameErr" class="text-danger">Apellido no valido</h6>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="number" class="col-md-8 control-label">Numero telefonico</label>
                            <input id="phone" type="text" class="form-control" name="phone"  required autofocus onclick="soloNumero()" onkeyup="soloNumero()" onkeypress="soloNumero()" onkeydown="soloNumero()">
                            <h6 hidden id="phoneErr" class="text-danger">Numero no valido</h6>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="address" class="col-md-6 control-label">Direccion</label>
                            <input id="address" type="text" class="form-control" name="address"  required autofocus>
                            <h6 hidden id="addressErr" class="text-danger"> Direccion no valida</h6>
                        </div>
                    </div>
                    <input type="hidden" name="id_usuario" id="id_usuario" value="{{Auth::user()->id}}">
                    <div class="col-sm-4" onmouseover="ValidarUsuario()">
                        <button id="btnCrearCliente" class="btn btn-success" type="submit" onclick="registroExitoso()">Crear</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
