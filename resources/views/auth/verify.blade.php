@extends('layouts.app')

@section('content')
    <div class="mt-5">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        <div class="h3 card-header">Verifica tu correo</div>
                        <div class="card-body">
                            @if (session('resent'))
                                <div class="alert alert-success" role="alert">
                                    Se ha enviado un nuevo enlace de verificación a su dirección de correo electrónico.
                                </div>
                            @endif
                            Antes de continuar, revise su correo electrónico para obtener un enlace de verificación.
                            Si no recibiste el correo electrónico, <a href="{{ route('verification.resend') }}">haga clic aquí para solicitar otro</a>.
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection
