@extends('layouts.app')
@section('name')
    <h4 style="color: green; font-size: 16px">Pinus<span
            style="color: orange">Art</span></h4>
    <b><a href="#quehacemos" style="font-size: 16px " class="mx-2"><span style="color: green">¿Que</span><span style="color: orange">Hacemos?</span></a></b>
    <b><a href="#contactanos" style="font-size: 16px" class="mx-2" ><span style="color: green">Contac</span><span style="color: orange">tanos...<span class="icon-phone"></span></span></a></b>
    <b><a href="/Galeria"  class="mx-2" style="font-size: 16px; " ><span style="color: green">Gale</span><span style="color: orange">ria<span class="icon-images mx-1"></span></span></a></b>
@endsection
@section('content')
    {{-- seccion de quienes somos --}}
    <section class="banner_area ban d-flex text-center" id="quehacemos">
        <div class="container align-self-center " >
            <div class="banner_content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="banner_content mt-3" style="margin-top: -200px">
                            <img src="{{asset('image/logo.png')}}" style="vertical-align: baseline; width: 200px; height:200px" alt="logo PinusArt" >
                            <h1 style="color: green">Pinus<span style="color: orange">Art</span></h1>
                            <div class="row">
                                <div class="col">
                                    <h2 class="mt-2" align="center">Mision</h2>
                                    @foreach($Proposito as $pro)
                                        <p class="card-text">{{$pro->mision}}</p>
                                    @endforeach
                                </div>
                                <div class="col">
                                    <h2 class="mt-2" align="center">Vision</h2>
                                    @foreach($Proposito as $pro)
                                        <p class="card-text">{{$pro->vision}}</p>
                                    @endforeach
                                </div>
                                <div class="col">
                                    <h2 class="mt-1" align="center">¿Que Hacemos?</h2>
                                    @foreach($Proposito as $pro)
                                        <p class="card-text">{{$pro->que_hacemos}}</p>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    {{-- termina seccion de quienes somos --}}
    <section>
        <div id="demo" class="carousel slide" data-ride="carousel">
            <!-- The slideshow -->
            <div class="container carousel-inner no-padding">
                <div class="carousel-item active">
                    @foreach($Productos as $pro)
                        <div class="col-xs-3 col-sm-3 col-md-3 mt-4">
                            <img class="" src="GaleriaProductos/{{$pro->photo}}" style="width: 90%; height: 200px">
                        </div>
                    @endforeach
                </div>
                <div class="carousel-item">
                    @foreach($Productos2 as $pro => $Productos2 )
                        @if($pro > 3 && $pro <8 )
                                <div class="col-xs-3 col-sm-3 col-md-3 mt-4">
                                    <img class="" src="{{asset('GaleriaProductos/'.$Productos2->photo)}}"style="width: 90%; height: 200px">
                                </div>
                        @endif
                    @endforeach
                </div>
            </div>
        </div>
        <br>
    </section>
    {{-- SECCION CONTACTANOS--}}
    <section class="banner_area d-flex text-center" id="contactanos">
        <div class="card col" style="max-width: 100%; max-height: 100%" id="wall">
            <div class="row no-gutters">
                <div class="col-md-4">
                    <div class="container">
                        <div>
                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d5607.9064835707495!2d-75.54695023523928!3d6.3436797722468246!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zNsKwMjAnMzcuMCJOIDc1wrAzMic0MC45Ilc!5e0!3m2!1ses!2sco!4v1577460126977!5m2!1ses!2sco"
                                    width="700" height="350" frameborder="0" style="border:0; padding: 55px" allowfullscreen="">
                            </iframe>
                        </div>

                    </div>
                </div>
                <div class="col-md-4" style="margin-left: 30%">
                    <div class="card-body " >
                        <div class="row col-md-12 mt-3" align="center">
                            <div class="col-md-4">
                                <div>
                                    <a href="https://www.facebook.com/pinus.art.52?epa=SEARCH_BOX"><span class="icon icon-facebook2 text-primary" style="font-size: 200%"></span></a>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div>
                                    <a href="https://www.instagram.com/grupopinusart/"><span class="icon icon-instagram text-dark" style="font-size: 200%"></span></a>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div>
                                    <a href="https://www.youtube.com"><span class="icon icon-youtube2 text-danger" style="font-size:200%"></span></a>
                                </div>
                            </div>
                        </div>
                        <form >
                            <h2 class="h1-responsive text text-center ">Contáctanos</h2>

                            <div class="form-group">
                                <input type="text" id="materialFormCardNameEx" class="form-control text-light">
                                <label for="materialFormCardNameEx" class="font-weight-light text-black">Nombre</label>
                            </div>
                            <!-- Material input email -->
                            <div class="form-group">
                                <input type="email" id="materialFormCardEmailEx" class="form-control text-light">
                                <label for="materialFormCardEmailEx" class="font-weight-light text-black">Correo</label>
                            </div>
                            <!-- Material textarea message -->
                            <div class="form-group">
                                <textarea type="text" id="materialFormMessageModalEx1" class="md-textarea form-control text-light"></textarea>
                                <label for="materialFormMessageModalEx1" class="text-black">Mensaje</label>
                            </div>
                            <button class="btn btn-block rounded-pill btn-success"
                                    type="submit">Enviar
                            </button>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    {{-- termina seccion contactanos --}}
    {{-- VENTANA MODAL DE REGISTRO --}}
    <div class="modal fade" id="modalRegister" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            {{-- contenido del modal --}}
            <div class="modal-content text-white" style="background: black;" id='ejemplo' >
                {{-- encabezado del modal --}}
                <div class="modal-header text-center">
                    <h4 class="modal-title w-100 font-weight-bold">{{ __('Register') }}</h4>
                    <h4 class="close" data-dismiss="modal" aria-label="Close"><span class="icon-undo2"></span></h4>
                </div>
                {{-- fin contenido del modal --}}
                {{-- VENTANA MODAL DE REGISTRO--}}
                <div class="modal-body">
                    <form method="POST" action="{{ route('register','#register') }}" id="asd" class="needs-validation" novalidate>
                        @csrf
                        <div class="form-group row{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 col-form-label text-md-right">Correo electronico</label>
                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required placeholder="@ email">
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 col-form-label text-md-right">Contraseña</label>
                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required placeholder="&#x1F512; contraseña">
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-rightl">Repetir contraseña</label>
                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required placeholder="&#x1F512; confirmar contraseña">
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    Registate
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            {{-- termina contenido del modal --}}
        </div>
    </div>
    {{-- fin del modal --}}
    {{-- VENTANA MODAL DE INICIO SESION --}}
    <div class="modal fade" id="modalRegisterForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            {{-- contenido del modal --}}
            <div class="modal-content text-white" style="background: black;"  id='ejemplo' >
                {{-- encabezado del modal --}}
                <div class="modal-header text-center">
                    <h4 class="modal-title w-100 font-weight-bold">{{ __('Login') }}</h4>
                    <h4 class="close" data-dismiss="modal" aria-label="Close"><span class="icon-undo2"></span></h4>
                    <hr>
                </div>
                {{-- fin contenido del modal --}}
                <div class="modal-body">
                    <br>
                    <form method="POST" action="{{ route('login','#login') }}" id="asd" class="needs-validation" novalidate>
                        @csrf
                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">Correo </label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}"  required autocomplete="email" autofocus placeholder="@ email">

                                @error('email')
                                <span class="invalid-feedback" role="alert">
                            <strong>el correo o la contraseña es incorrecto</strong>
                        </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">Contraseña</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" placeholder="&#x1F512; contraseña">

                                @error('password')
                                <span class="invalid-feedback" role="alert">
                            <strong>la contraseña es incorrecta</strong>
                        </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Login') }}
                                </button>

                                @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        Olvidaste tu contraseña?
                                    </a>
                                @endif
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            {{-- termina contenido del modal --}}
        </div>
    </div>
    {{-- fin modal del inicio de sesion --}}

@endsection
@section('script')
    <script>

        if(window.location.hash === '#login')
        {
            $('#modalRegisterForm').modal('show');
        }
        $('#modalRegisterForm').on('hide.bs.modal', function(){
            window.location.hash = '#';
        });
        $('#exampleModalLong').on('hide.bs.modal', function(){
            window.location.hash = '#';
        });
        $('#myModal').on('hide.bs.modal', function(){
            window.location.hash = '#';
        });

        if(window.location.hash === '#register')
        {
            $('#modalRegister').modal('show');
        }
        $('#modalRegister').on('hide.bs.modal', function(){
            window.location.hash = '#1';
        });
        $('#exampleModalLong').on('hide.bs.modal', function(){
            window.location.hash = '#1';
        });
        $('#myModal').on('hide.bs.modal', function(){
            window.location.hash = '#1';
        });

    </script>
    <style>
        .col-md-3{
            display: inline-block;
            margin-left:-4px;
        }
        .col-md-3 img{
            width:100%;
            height:auto;
        }
        body .carousel-indicators li{
            background-color:red;
        }
        body .carousel-control-prev-icon,
        body .carousel-control-next-icon{
            background-color:red;
        }
        body .no-padding{
            padding-left: 0;
            padding-right: 0;
        }
        .card {
            background-image: url('/image/fondo232.jpeg');
            Background-size:100%;
            Background-repeat:no-repeat;
        }
        .banner_area{

        }
    </style>
@endsection
