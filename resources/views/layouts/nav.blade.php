
@include('layouts.link')
<link href="{{asset('css/sb-admin-2.min.css')}}" rel="stylesheet">
<body>
<div style="border-color: gold">
    <div class="borderdd" id="wrapper">
        <!-- Sidebar -->
        <ul class="navbar-nav bg-gradient-dark sidebar sidebar-dark">
            <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
                <div class="sidebar-brand-icon rotate-n-15"></div>
                <div class="h3 sidebar-brand-text mx-2">
                    <img class="avatar mt-5" src="./image/logo.png"
                         style="border-radius: 20px 20px; width: 50%; height: 50; ">
                </div>
            </a>
            <hr class="sidebar-divider my-0 mt-5" style="border-color: gold">
            <li class="nav-item ">
                <a class="nav-link" href="/Cliente">
                    <span class="icon-home"></span>
                    <span>Inicio</span></a>
            </li>
            <!-- Divider -->
            <hr class="sidebar-divider">
            <!-- Nav Item - Pages Collapse Menu -->
            <li class="nav-item">
                <a class="nav-link" href="/CatalogoProductos">
                    <span class="icon-images"></span>
                    <span>Catalago</span>
                </a>

            </li>
            <hr class="sidebar-divider">

            <li class="nav-item">
                <a class="nav-link" href="/ProductoFinal">
                    <span class="icon-paste"></span>
                    <span>Ficha Tecnica</span>
                </a>
            </li>
            <hr class="sidebar-divider">
            <li class="nav-item">
                <a class="nav-link" href="/Pedido">
                    <span class="icon-clipboard"></span>
                    <span>Pedido</span></a>
            </li>

            <hr class="sidebar-divider d-none d-md-block">
        </ul>
        <div id="content-wrapper" class="d-flex flex-column">
            <div class="contentFondo" id="content">
                <nav class="navbar navbar-expand navbar-light  topbar mb-4 static-top shadow pepe " id="wallpaper" >
                    <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                        <i class="fa fa-bars"></i>
                    </button>
                    @yield('buscar')
@yield('info')
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item dropdown no-arrow d-sm-none">
                            <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button"
                               data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fas fa-search fa-fw"></i>
                            </a>
                            <!-- Dropdown - Messages -->
                            <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in"
                                 aria-labelledby="searchDropdown">
                                <form class="form-inline mr-auto w-100 navbar-search">
                                    <div class="input-group">
                                        <input type="text" class="form-control bg-light border-0 small"
                                               placeholder="Search for..." aria-label="Search"
                                               aria-describedby="basic-addon2">
                                        <div class="input-group-append">
                                            <button class="btn btn-primary" type="button">
                                                <i class="fas fa-search fa-sm"></i>
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </li>
                        <div class="topbar-divider d-none d-sm-block" style="border-color: gold"></div>
                        @guest
                            <li><a class="btn btn-sm btn-success mx-1" href="{{ route('login') }}">Login</a></li>
                            <li><a class="btn btn-sm btn-info mx-1" href="{{ route('register') }}">Registrar</a></li>
                        @else
                            <li class="nav-item dropdown no-arrow">
                                <a class="nav-link dropdown-toggle " href="#" id="userDropdown" role="button"
                                   data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">

                                    @yield('name')


                                </a>
                                <!-- Dropdown - User Information -->
                                <div
                                    class="dropdown-menu dropdown-menu-right bg-dark shadow animated--grow-in text-white"
                                    aria-labelledby="userDropdown">
                                    <a class="dropdown-item" href="/Perfil">
                                        <i class="icon icon-user"></i>
                                        Perfil
                                    </a>
                                    <a class="dropdown-item" href="{{ route('logout') }}"

                                       onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();" data-toggle="modal"
                                       data-target="#logoutModal">
                                        <i class="icon icon-enter"></i>
                                        Cerrar sesion
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                          style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </nav>
                <div class="">
                    @yield('contenidoNav')
                </div>
            </div>
        </div>
    </div>
</div>
<style>
    .topbar-divider {
        background-color: gold !important;
    }

    .sidebar-divider {
        background-color: gold;

    }

    .pepe {
        height: 50px;
        background: -webkit-gradient(linear, left top, right top, from(rgba(4, 9, 30, 0.8)), to(rgba(4, 9, 30, 0.8))), url("../image/fondo2.jpg");
        background: -webkit-linear-gradient(left, rgba(4, 9, 30, 0.8), rgba(4, 9, 30, 0.8)), url("../image/fondo2.jpg");
        background: -o-linear-gradient(left, rgba(4, 9, 30, 0.8), rgba(4, 9, 30, 0.8)), url("../image/fondo2.jpg");
        background: linear-gradient(to right, rgba(4, 9, 30, 0.8), rgba(4, 9, 30, 0.8)), url("../image/fondo2.jpg");
        background-size: cover;
        z-index: 1;
        border-color: gold;
        border-bottom: solid;
        border-width: 0.7px;
        border-color: gold;
    }

    .contentFondo {
        background: -webkit-gradient(linear, left top, right top, from(rgba(4, 9, 30, 0.8)), to(rgba(4, 9, 30, 0.8))), url("../image/f.jpg");
        background: -webkit-linear-gradient(left, rgba(4, 9, 30, 0.8), rgba(4, 9, 30, 0.8)), url("../image/f.jpg");
        background: -o-linear-gradient(left, rgba(4, 9, 30, 0.8), rgba(4, 9, 30, 0.8)), url("../image/f.jpg");
        background: linear-gradient(to right, rgba(4, 9, 30, 0.8), rgba(4, 9, 30, 0.8)), url("../image/f.jpg");
        background-size: cover;
    }

    .bg-gradient-dark {

        background: -webkit-gradient(linear, left top, right top, from(rgba(4, 9, 30, 0.8)), to(rgba(4, 9, 30, 0.8))), url("../image/fondo2.jpg");
        background: -webkit-linear-gradient(left, rgba(4, 9, 30, 0.8), rgba(4, 9, 30, 0.8)), url("../image/fondo2.jpg");
        background: -o-linear-gradient(left, rgba(4, 9, 30, 0.8), rgba(4, 9, 30, 0.8)), url("../image/fondo2.jpg");
        background: linear-gradient(to right, rgba(4, 9, 30, 0.8), rgba(4, 9, 30, 0.8)), url("../image/fondo2.jpg");
        background-size: cover;
        z-index: 1;
        position: relative;
        width: 100%;

        border-color: gold;
    }

    .bg-gradient-primary {
        background-color: #5a5c69 !important;
        background-size: cover !important;
    }

    .nav-link span {
        font-size: 18px !important;
    }

    .nav-link {
        color: #ffffff !important
    }

    .nav-link:hover {
        color: white !important;
        background: #0b2e13;
    }


</style>
<!-- End of Page Wrapper -->

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>
<!-- Logout Modal-->
@include('layouts.scrpt')
@yield('script')
</body>

