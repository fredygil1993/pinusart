@include('layouts.link')
@include('layouts.scrpt')
    <div id="app">
        <header>
            <nav class="navbar navbar-expand-lg navbar-light bg-dark  fixed-top col-xs col-sm col-md col-lg">
                <a href="#top" class="logoPlaneem d-flex justify-content-center align-items-center mr-2"> </a>
                    <div class="boxSep mr-2">
                        <div class="imgLiquidFill imgLiquid ">
                            <img src="{{asset('image/logo.png')}}" style="vertical-align: baseline; width: 30px; height:30px" alt="logo PinusArt">
                        </div>
                    </div>
                    @yield('name')
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup"
                        aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                @yield('info')
                <div class="navbar-collapse ">
                    <ul class="navbar-nav ml-auto mr-md-3">
                        @guest
                            <b><li><a class=" mx-1"style="font-size: 16px " data-toggle="modal"
                                      data-target="#modalRegisterForm"  href="#"><span style="color: green">Ingresar</span> </a></li></b>
                            <li>/</li>
                            <b><li><a class=" mx-1" style="font-size: 16px "data-toggle="modal"
                                      data-target="#modalRegister"  href="#1"><span style="color: orange"> Registrar</span></a></li></b>
                        @else
                            <li class="nav-item dropdown no-arrow ">
                                <a class=" k  nav-link dropdown-toggle " href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">

                                    <span class="icon-switch text-white" style="color: white"></span>
                                </a>
                                <!-- Dropdown - User Information -->
                                <div class="dropdown-menu bg-dark dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">

                                    <div class="dropdown-divider" style="border-color: gold;"></div>
                                    <a class="dropdown-item  " style="color: white" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();" data-toggle="modal" data-target="#logoutModal">
                                        <i class="icon icon-enter"></i>
                                         Cerrar sesion
                                    </a>
                                    <div class="dropdown-divider"style="border-color: gold;"></div>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </nav>
        </header>
    </div>
    @yield('content')
    <style>
        #liveclock{
            margin-left: 1%;
        }
    </style>
    @yield('script')






