@extends('layouts.app')
@section('name')
    <h4 style="color: green">Adminis<span style="color: orange">trador</span></h4>
@endsection
@section('info')
    <div class="collapse navbar-collapse " id="navbarNavAltMarkup">
        <div class="navbar-nav ">
            <a class="nav-item nav-link" style="color: white" data-scroll href="/home"><span class="icon-home"></span> Inicio</a>
            <div class="dropdown">
                <a class="nav-item nav-link dropdown-toggle" style="color: white" data-scroll href="/Productos"  id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <span class="icon-images"></span> Productos
                </a>
                <div class="dropdown-menu bg-dark" aria-labelledby="dropdownMenuButton">
                    <div class="dropdown-divider" style="border-color: gold;"></div>
                    <a class="dropdown-item " style="color: white"href="/CategoriaProductos"><span class="icon-cogs"></span> Categorias</a>
                    <div class="dropdown-divider" style="border-color: gold;"></div>
                    <a class="dropdown-item" style="color: white" href="/Productos"><span class="icon-images"></span> Productos</a>
                    <div class="dropdown-divider" style="border-color: gold;"></div>
                </div>
            </div>
            <div class="dropdown">
                <a class="nav-item nav-link dropdown-toggle" style="color: white" data-scroll href="/Productos"  id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <span class="icon-list2"></span> Insumos
                </a>
                <div class="dropdown-menu bg-dark" aria-labelledby="dropdownMenuButton">
                    <div class="dropdown-divider" style="border-color: gold;"></div>
                    <a class="dropdown-item"style="color: white" href="/TipoInsumos"><span class="icon-hammer"></span> Tipo Insumos</a>
                    <div class="dropdown-divider" style="border-color: gold;"></div>
                    <a class="dropdown-item"style="color: white" href="/Insumos"><span class="icon-droplet"></span> Insumos</a>
                    <div class="dropdown-divider" style="border-color: gold;"></div>
                    <a class="dropdown-item" style="color: white"href="/Tarifas"><span class="icon-lastfm"></span> Tarifa Insumos</a>
                    <div class="dropdown-divider" style="border-color: gold;"></div>
                </div>
            </div>
            <div class="dropdown">
                <a class="nav-item nav-link" style="color: white" data-scroll href="/InformeClientes"><span class="icon-cogs"></span> Informes</a>
            </div>
        </div>
    </div>
@endsection
@section('content')
    <div class="modal fade"  id="modalProduct" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog  modal-lg modal-dialog-centered" role="document">
            {{-- contenido del modal --}}
            <div class="modal-content col-xl-12" style="background: black; opacity: 0.8" id='ejemplo' >
                {{-- encabezado del modal --}}
                <div class="modal-header text-center text-white">
                    <h4 class="modal-titlew-100 font-weight-bold">{{ __('Producto Nuevo') }}</h4>
                    <a class="close" data-dismiss="modal" aria-label="Close"><span class="icon-undo2"></span></a>
                    <hr>
                </div>
                <div class="modal-body text-white" >
                    <form class="" method="POST" action="{{route('Productos.store')}}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="form-row">
                            <div class="form-group col-md-4">
                                <label for="">Nombre Producto</label>
                                <input id="nombre" type="text" class="form-control" name="nombre" required autofocus>
                                <h6 hidden id="nombreErr" style="margin-top: 2%" class="text-danger">Nombre no valido</h6>

                            </div>
                            <div class="form-group col-md-4">
                                <label for="">Categoria Producto</label>
                                <br>
                                <select class="form-control" name="categoria" id="categoria" required autofocus|>
                                    <option value="">Seleccionar</option>
                                    @foreach($categorias as $cate)
                                        <option value="{{$cate->id}}">{{$cate->nameCategory}}</option>
                                    @endforeach
                                </select>
                                <h6 hidden id="categoriaErr" style="margin-top: 2%" class="text-danger">Categoria no valida</h6>
                            </div>
                            <div class="form-group col-md-4">
                                <label for="">tamaño</label>
                                <select name="tamaño"  id="tamaño" class="form-control" required autofocus >
                                    <option value="">Seleccionar</option>
                                    <option value="grande">Grande</option>
                                    <option value="medioano">Medioano</option>
                                    <option value="pequeño">Pequeño</option>
                                </select>
                                <h6 hidden id="tamañoErr" style="margin-top: 2%" class="text-danger">Tamaño no valido</h6>
                            </div>
                            <div class="form-group col-md-4">
                                <label for="">Ancho</label>
                                <input id="ancho" type="text" class="form-control" name="ancho"  required autofocus onkeydown="soloNumero()" onkeypress="soloNumero()" onkeyup="soloNumero()">
                                <h6 hidden id="anchoErr" style="margin-top: 2%" class="text-danger">Ancho no valido</h6>
                            </div>
                            <div class="form-group col-md-4">
                                <label for="">Profundidad</label>
                                <input id="profundidad" type="text" class="form-control" name="profunfifaf"  required autofocus onkeydown="soloNumero()" onkeypress="soloNumero()" onkeyup="soloNumero()">
                                <h6 hidden id="profundidadErr" style="margin-top: 2%" class="text-danger">Profundidad no valida</h6>
                            </div>
                            <div class="form-group col-md-4">
                                <label for="">Alto</label>
                                <input id="alto" type="text" class="form-control" name="alto"  required autofocus onkeydown="soloNumero()" onkeypress="soloNumero()" onkeyup="soloNumero()">
                                <h6 hidden id="altoErr" style="margin-top: 2%" class="text-danger">Alto no valido</h6>
                            </div>
                            <div class="form-group col-md-4">
                                <label for="">Año</label>
                                <select id="año" class="form-control" name="año"  required onclick="añoselect()">
                                    <option value="">Seleccione </option>
                                </select>
                                <h6 hidden id="añoErr" style="margin-top: 2%" class="text-danger">Año no valido</h6>
                            </div>
                            <div class="form-group col-md-4">
                                <label for="">Valor</label>
                                <input id="precio" type="text" class="form-control" name="precio" value="{{ old('value_unit') }}" required autofocus onkeydown="soloNumero()" onkeypress="soloNumero()" onkeyup="soloNumero()">
                                <h6 hidden id="precioErr" style="margin-top: 2%" class="text-danger">Precio no valido</h6>
                            </div>
                            <div class="form-group col-md-4">
                                <label for="">valor por Mayor</label>
                                <input id="precio2" type="text" class="form-control" name="precio2"  required autofocus onkeydown="soloNumero()" onkeypress="soloNumero()" onkeyup="soloNumero()">
                                <h6 hidden id="precio2Err" style="margin-top: 2%" class="text-danger">Precio al mayor invalido</h6>
                            </div>
                        </div>

                        <div class="form-group">
                            <input type="file" name="imagen" id="imagen" >
                            <h6 hidden id="imagenErr" style="margin-top: 2%" class="text-danger">Imagen no valida</h6>
                        </div>
                        <div class="form-group">
                            <label for="">Descripcion</label>
                            <textarea  type="text"name="descripcion" id="descripcion" class="form-control" cols="5" rows="2"></textarea>
                            <h6 hidden id="descripcionErr" style="margin-top: 2%" class="text-danger">Descripcion No valida</h6>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-4" onmouseover="valProductoNuevo()">
                                <button class="btn text-black btn-sm btn-outline-success" type="submit" id="buttonRegistrar" name="buttonRegistrar"  onclick="registroExitoso()"> Registrar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class=" container mt-5">
        <table id="tablep" class="table-striped table-hover prueba" >
            <thead class="text-white" style="background-color: #0a0a0a">
            <tr>
                <th scope="col" hidden>id</th>
                <th scope="col">Imagen</th>
                <th scope="col">Nombre</th>
                <th scope="col">Categoria Producto</th>
                <th scope="col">Tamaño</th>
                <th scope="col">Medidas</th>
                <th scope="col">Año</th>
                <th scope="col">Valor Unidad</th>
                <th scope="col">Valor Al Por Mayor</th>
                <th scope="col">Descripcion</th>
                <th scope="col">Estado</th>
                <th scope="col">Opciones
                </th>
                <a class="btn btn-success" data-toggle="modal"
                   data-target="#modalProduct"  href="#" style="background-color: green"><span class="icon-plus"></span>
                </a>
            </tr>
            </thead >
            <tbody>
            @foreach($productos as $producto)
                <tr >
                    <td hidden>{{$producto->id}}</td>
                    <th><img class="" src="GaleriaProductos/{{$producto->photo}}" style="width: 50px; height: 50px"></th>
                    <td style="color: #0a0a0a">{{$producto->name_product}}</td>
                    <td style="color: #0a0a0a">{{$producto->nameCategory}}</td>
                    <td style="color: #0a0a0a">{{$producto->size}}</td>
                    <td style="color: #0a0a0a">{{$producto->high}} x {{$producto->width}} x {{$producto->depth}} cm </td>
                    <td style="color: #0a0a0a">{{$producto->year}}</td>
                    <td style="color: #0a0a0a">{{$producto->value_unit}}</td>
                    <td style="color: #0a0a0a">{{$producto->value_max12}}</td>
                    <td style="color: #0a0a0a">{{$producto->description}}</td>
                    <td style="color: #0a0a0a">{{$producto->state}}</td>
                    <td style="color: #0a0a0a"><a class="btn btn-warning" data-toggle="modal" data-target="#EditarModal" onclick="EditarModalFunction({{$producto->id}})"><span class="icon-pencil2"></span></a>
                    <a class="btn btn-danger text-white" data-toggle="modal" data-target="#EditarModal"><span class="icon-minus"></span></a>
                    </td>
                    {{--  href="{{URL::action('ProductsController@edit',$producto->id)}}" --}}
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    <!--Modal Editar-->
    <div class="modal fade" id="EditarModal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content" style="background: black; opacity: 0.8">
                <div class="modal-header text-center text-white">
                    <h5 class="modal-title">Editar Producto</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-white" id="CuerpoModal"></div>
            </div>
        </div>
    </div>
    <!--Modal Editar-->
@endsection
@section('script')
    <script>
        {{--funcion modal edit--}}
        function EditarModalFunction(data) {
            $.ajax({
                url:"/Productos/"+data+"/edit",
                type:'GET',
                cache:false,
                success: function (result) {
                    $('#CuerpoModal').html(result);
                }
            });

        }
        {{--funcion modal edit--}}

        $(document).ready(function () {


            $('#tablep').DataTable({
                language: {
                    "decimal": "",
                    "emptyTable": "No hay información",
                    "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
                    "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
                    "infoFiltered": "(Filtrado de _MAX_ total entradas)",
                    "infoPostFix": "",
                    "thousands": ",",
                    "lengthMenu": "Mostrar _MENU_ Entradas",
                    "loadingRecords": "Cargando...",
                    "processing": "Procesando...",
                    "search": "Buscar:",
                    "zeroRecords": "Sin resultados encontrados",
                    "paginate": {
                        "first": "Primero",
                        "last": "Ultimo",
                        "next": "Siguiente",
                        "previous": "Anterior"
                    }
                },
                paging:false,
                bFilter:false,
                ordering:true,
                searching:true,
                info:false,
                scrollY:        "380px",
                scrollCollapse: true,
            });
        });

        if(window.location.hash === '#Products')
        {
            $('#modalProduct').modal('show');
        }
        $('#modalProduct').on('hide.bs.modal', function(){
            window.location.hash = '#';
        });
        $('#exampleModalLong').on('hide.bs.modal', function(){
            window.location.hash = '#';
        });
        $('#myModal').on('hide.bs.modal', function(){
            window.location.hash = '#';
        });
    </script>
@endsection
