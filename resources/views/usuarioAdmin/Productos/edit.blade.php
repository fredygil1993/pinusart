<form class="" method="POST" action="{{route('Productos.update',$producto->id)}}" enctype="multipart/form-data">
    {{ csrf_field() }}
    @method('PUT')
    <div class="form-row">
        <div class="form-group col-md-4">
            <label for="">Nombre Producto</label>
            <input id="nombre" type="text" class="form-control" name="nombre" required autofocus value="{{$producto->name_product}}">
            <h6 hidden id="nombreErr" style="margin-top: 2%" class="text-danger">Nombre no valido</h6>

        </div>
        <div class="form-group col-md-4">
            <label for="">Categoria Producto</label>
            <br>
            <select class="form-control" name="categoria" id="categoria" required autofocus|>
                <option value="">Seleccionar</option>
                @foreach($categorias as $cate)
                    <option value="{{$cate->id}}">{{$cate->nameCategory}}</option>
                @endforeach
            </select>
            <h6 hidden id="categoriaErr" style="margin-top: 2%" class="text-danger">Categoria no valida</h6>
        </div>
        <div class="form-group col-md-4">
            <label for="">Estado</label>
            <select name="estado"  id="estado" class="form-control" required autofocus >
                <option value="">Seleccionar</option>
                <option value="1">Activo</option>
                <option value="0">Inactivo</option>
            </select>
            <h6 hidden id="tamañoErr" style="margin-top: 2%" class="text-danger">Tamaño no valido</h6>
        </div>
        <div class="form-group col-md-4">
            <label for="">tamaño</label>
            <select name="tamaño"  id="tamaño" class="form-control" required autofocus >
                <option value="">Seleccionar</option>
                <option value="grande">Grande</option>
                <option value="medioano">Medioano</option>
                <option value="pequeño">Pequeño</option>
            </select>
            <h6 hidden id="tamañoErr" style="margin-top: 2%" class="text-danger">Tamaño no valido</h6>
        </div>
        <div class="form-group col-md-4">
            <label for="">Alto</label>
            <input id="alto" type="text" class="form-control" name="alto"  required autofocus onkeydown="soloNumero()" onkeypress="soloNumero()" onkeyup="soloNumero()" value="{{$producto->high}}">
            <h6 hidden id="altoErr" style="margin-top: 2%" class="text-danger">Alto no valido</h6>
        </div>
        <div class="form-group col-md-4">
            <label for="">Ancho</label>
            <input id="ancho" type="text" class="form-control" name="ancho"  required autofocus onkeydown="soloNumero()" onkeypress="soloNumero()" onkeyup="soloNumero()" value="{{$producto->depth}}">
            <h6 hidden id="anchoErr" style="margin-top: 2%" class="text-danger">Ancho no valido</h6>
        </div>
        <div class="form-group col-md-4">
            <label for="">Año</label>
            <select id="año" class="form-control" name="año"  required onclick="añoselect()">
                <option value="">Seleccione </option>
            </select>
            <h6 hidden id="añoErr" style="margin-top: 2%" class="text-danger">Año no valido</h6>
        </div>
        <div class="form-group col-md-4">
            <label for="">Valor</label>
            <input id="precio" type="text" class="form-control" name="precio" value="{{$producto->value_unit}}" required autofocus onkeydown="soloNumero()" onkeypress="soloNumero()" onkeyup="soloNumero()">
            <h6 hidden id="precioErr" style="margin-top: 2%" class="text-danger">Precio no valido</h6>
        </div>
        <div class="form-group col-md-4">
            <label for="">valor por Mayor</label>
            <input id="precio2" type="text" class="form-control" name="precio2"  required autofocus onkeydown="soloNumero()" onkeypress="soloNumero()" onkeyup="soloNumero()" value="{{$producto->value_max12}}">
            <h6 hidden id="precio2Err" style="margin-top: 2%" class="text-danger">Precio al mayor invalido</h6>
        </div>
    </div>

    <!--div class="form-group">
        <input type="file" name="imagen" id="imagen" >
        <h6 hidden id="imagenErr" style="margin-top: 2%" class="text-danger">Imagen no valida</h6>
    </div-->
    <div class="form-group">
        <label for="">Descripcion</label>
        <textarea  type="text"name="descripcion" id="descripcion" class="form-control" cols="5" rows="2"  value="{{$producto->description}}"></textarea>
        <h6 hidden id="descripcionErr" style="margin-top: 2%" class="text-danger">Descripcion No valida</h6>
    </div>
    <div class="form-group row">
        <div class="col-sm-4" onmouseover="valProductoNuevo()">
            <button class="btn text-black btn-sm btn-outline-success" type="submit" id="buttonRegistrar" name="buttonRegistrar"  onclick="registroExitoso()">Editar</button>
        </div>
    </div>
</form>
