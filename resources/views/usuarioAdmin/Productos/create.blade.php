@extends('layouts.app')
@section('name')
        <h4 style="color: green">Adminis<span style="color: orange">trador</span></h4>
@endsection
@section('info')
    <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
        <div class="navbar-nav">
            <a class="nav-item nav-link" data-scroll href="/home"><span class="icon-home"></span> Home</a>
            <a class="nav-item nav-link" data-scroll href="/Productos"><span class="icon-images"></span> Productos</a>
            <a class="nav-item nav-link" data-scroll href="/Categorias"><span class="icon-cogs"></span> Categoria Insumos </a>
            <a class="nav-item nav-link" data-scroll href="/Insumos"><span class="icon-hammer"></span> Insumos</a>
            <a class="nav-item nav-link" data-scroll href="/Pedidos"><span class="icon-cart"></span> Pedios</a>
            <a class="nav-item nav-link" data-scroll href="/Pedidos"><span class="icon-clipboard"></span> Ficha Tecnica</a>
        </div>
    </div>
@endsection
@section('content')
    <div class="mt-5">
        <div class="container" style="opacity: 0.7">
            <div class="row">
                <div class="col-sm-4"></div>
                <div class="col-md-4">
                    <div class="card text-white bg-dark">
                        <div class="card-header card-header" >
                            <h4 class="card-title">Registrar Producto</h4>
                        </div>
                        <div class="card-body">
                            <form class="" method="POST" action="{{route('Productos.store')}}" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <input type="file" name="imagen" id="imagen" >
                                </div>
                                <div class="form-group">
                                    <label for="">Nombre </label>
                                    <input type="text" name="nombre" id="nombre" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label for="">Valor Unidad</label>
                                    <input type="number" name="precio" id="valor" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label for="">Valor Por Mayor</label>
                                    <input type="number" name="precio+12" id="precio+12" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label for="">Descripcion</label>
                                    <textarea  type="text"name="descripcion" id="descripcion" class="form-control" cols="5" rows="2" required></textarea>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-4">
                                        <button class="btn text-black btn-sm btn-outline-success" type="submit" id="buttonRegistrar" name="buttonRegistrar"> Registrar</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <style>
        body{
            background-image: url("/image/fondo2.jpg");
            Background-size:100%;
            Background-repeat:no-repeat;
        }
    </style>
@endsection



