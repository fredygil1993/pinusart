
        <form class="" method="POST" action="{{route('CategoriaProductos.update', $Categories->id)}}">
            {{ csrf_field() }}
            @method("PUT")
            <div class="form-group" >
                <label for="">Tipo Categoria</label>
                <input type="text" name="nameCategory" class="form-control" value="{{$Categories->nameCategory}}">
                <h6 hidden id="categoriaErr" style="margin-top: 2%" class="text-danger">Categoria invalida</h6>
            </div>
            <div>
                <label for="">Estado</label>
                <select name="tipoestado" id="tipoestado">
                    <option value="">Seleccione</option>

                </select>
            </div>
            <div onmouseover="valCategoria()">
                <button class="btn btn-outline-success" type="submit" id="btnCrearCatego" onclick="registroExitoso()">Editar</button>
            </div>
        </form>

