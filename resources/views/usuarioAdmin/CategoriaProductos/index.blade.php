@extends('layouts.app')
@section('name')
    <h4 style="color: green">Adminis<span style="color: orange">trador</span></h4>
@endsection
@section('info')
    <div class="collapse navbar-collapse " id="navbarNavAltMarkup">
        <div class="navbar-nav ">
            <a class="nav-item nav-link" style="color: white" data-scroll href="/home"><span class="icon-home"></span> Inicio</a>
            <div class="dropdown">
                <a class="nav-item nav-link dropdown-toggle" style="color: white" data-scroll href="/Productos"  id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <span class="icon-images"></span> Productos
                </a>
                <div class="dropdown-menu bg-dark" aria-labelledby="dropdownMenuButton">
                    <div class="dropdown-divider" style="border-color: gold;"></div>
                    <a class="dropdown-item " style="color: white"href="/CategoriaProductos"><span class="icon-cogs"></span> Categorias</a>
                    <div class="dropdown-divider" style="border-color: gold;"></div>
                    <a class="dropdown-item" style="color: white" href="/Productos"><span class="icon-images"></span> Productos</a>
                    <div class="dropdown-divider" style="border-color: gold;"></div>
                </div>
            </div>
            <div class="dropdown">
                <a class="nav-item nav-link dropdown-toggle" style="color: white" data-scroll href="/Productos"  id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <span class="icon-list2"></span> Insumos
                </a>
                <div class="dropdown-menu bg-dark" aria-labelledby="dropdownMenuButton">
                    <div class="dropdown-divider" style="border-color: gold;"></div>
                    <a class="dropdown-item"style="color: white" href="/TipoInsumos"><span class="icon-hammer"></span> Tipo Insumos</a>
                    <div class="dropdown-divider" style="border-color: gold;"></div>
                    <a class="dropdown-item"style="color: white" href="/Insumos"><span class="icon-droplet"></span> Insumos</a>
                    <div class="dropdown-divider" style="border-color: gold;"></div>
                    <a class="dropdown-item" style="color: white"href="/Tarifas"><span class="icon-lastfm"></span> Tarifa Insumos</a>
                    <div class="dropdown-divider" style="border-color: gold;"></div>
                </div>
            </div>
            <div class="dropdown">
                <a class="nav-item nav-link" style="color: white" data-scroll href="/InformeClientes"><span class="icon-cogs"></span> Informes</a>
            </div>
        </div>
    </div>
@endsection
@section('content')
    <div class="container mt-5">
    <div class="modal fade"  id="modalCategory" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="col-sm-2"></div>
            {{-- contenido del modal --}}
            <div class="modal-content col-sm-9" style="background: black; opacity: 0.8" id='ejemplo' >
                {{-- encabezado del modal --}}
                <div class="modal-header text-center text-white">
                    <h4 class="modal-title w-100 font-weight-bold">{{ __('Categoria Productos') }}</h4>
                    <a class="close" data-dismiss="modal" aria-label="Close"><span class="icon-undo2"></span></a>
                </div>
                <div class="modal-body text-white" >
                    <form class="" method="POST" action="{{route('CategoriaProductos.store')}}">
                        {{ csrf_field() }}
                        <div class="form-group" >
                            <label for="">Tipo Categoria</label>
                            <input type="text" name="categoria" id="categoria" class="form-control">
                            <h6 hidden id="categoriaErr" style="margin-top: 2%" class="text-danger">Error, elija una categoria </h6>
                        </div>
                        <div onmouseover="valCategoria()">
                            <button class="btn btn-outline-success" type="submit" id="btnCrearCatego" onclick="registroExitoso()">Crear</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="mt-3">
        <div class="container">
            <table id="tablep" class="table-striped table-hover  prueba" >
                <thead class="text-white" style="background-color: #0a0a0a" style="color: #0a0a0a">
                <tr>
                    <th scope="col-4">ID</th>
                    <th scope="col-4">Tipo Categoria</th>
                    <th scope="col-4">Opciones</th>
                    <a class="btn text-white" data-toggle="modal"
                       data-target="#modalCategory" style="background-color: green" href="#"><span class="icon-plus"></span></a>
                </tr>
                </thead>
                <tbody>
                @foreach($Categories as $categoria)
                    <tr>
                        <td style="color: #0a0a0a">{{$categoria->id}}</td>
                        <td style="color: #0a0a0a">{{$categoria->nameCategory}}</td>
                        <td style="color: #0a0a0a"><a class="btn btn-warning" data-toggle="modal" data-target="#EditarModal" onclick="EditarModalFunction({{$categoria->id}})" ><span class="icon-pencil2"></span></a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

    {{--href="{{URL::action('ProductCategoryController@edit',$categoria->id)}}" --}}

    <!--Modal Editar-->
    <div class="modal fade" id="EditarModal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content" style="background: black; opacity: 0.8">
                <div class="modal-header text-center text-white">
                    <h5 class="modal-title">Editar Categoría</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-white" id="CuerpoModal"></div>
            </div>
        </div>
    </div>
    <!--Modal Editar-->
@endsection
@section('script')
    <script>

       {{--funcion modal edit--}}
        function EditarModalFunction(data) {
            $.ajax({
                url:"/CategoriaProductos/"+data+"/edit",
                type:'GET',
                cache:false,
                success: function (result) {
                    $('#CuerpoModal').html(result);
                }
            });

        }
       {{--funcion modal edit--}}
        $(document).ready(function () {
            $('#tablep').DataTable({
                language: {
                    "decimal": "",
                    "emptyTable": "No hay información",
                    "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
                    "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
                    "infoFiltered": "(Filtrado de _MAX_ total entradas)",
                    "infoPostFix": "",
                    "thousands": ",",
                    "lengthMenu": "Mostrar _MENU_ Entradas",
                    "loadingRecords": "Cargando...",
                    "processing": "Procesando...",
                    "search": "Buscar:",
                    "zeroRecords": "Sin resultados encontrados",
                    "paginate": {
                        "first": "Primero",
                        "last": "Ultimo",
                        "next": "Siguiente",
                        "previous": "Anterior"
                    }
                },
                paging: false,
                bFilter: false,
                ordering: true,
                searching: true,
                info: false,
                scrollY:        "400px",
                scrollCollapse: true,
            });
        });
        if(window.location.hash === '#Catgory')
        {
            $('#modalCatgory').modal('show');
        }
        $('#modalCategory').on('hide.bs.modal', function(){
            window.location.hash = '#';
        });
        $('#exampleModalLong').on('hide.bs.modal', function(){
            window.location.hash = '#';
        });
        $('#myModal').on('hide.bs.modal', function(){
            window.location.hash = '#';
        });

    </script>
@endsection
