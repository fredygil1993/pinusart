@extends('layouts.app')
@section('name')
    <h4 style="color: green">Adminis<span style="color: orange">trador</span></h4>
@endsection
@section('info')
    <div class="collapse navbar-collapse " id="navbarNavAltMarkup">
        <div class="navbar-nav ">
            <a class="nav-item nav-link" style="color: white" data-scroll href="/home"><span class="icon-home"></span> Inicio</a>
            <div class="dropdown">
                <a class="nav-item nav-link dropdown-toggle" style="color: white" data-scroll href="/Productos"  id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <span class="icon-images"></span> Productos
                </a>
                <div class="dropdown-menu bg-dark" aria-labelledby="dropdownMenuButton">
                    <div class="dropdown-divider" style="border-color: gold;"></div>
                    <a class="dropdown-item " style="color: white"href="/CategoriaProductos"><span class="icon-cogs"></span> Categorias</a>
                    <div class="dropdown-divider" style="border-color: gold;"></div>
                    <a class="dropdown-item" style="color: white" href="/Productos"><span class="icon-images"></span> Productos</a>
                    <div class="dropdown-divider" style="border-color: gold;"></div>
                </div>
            </div>
            <div class="dropdown">
                <a class="nav-item nav-link dropdown-toggle" style="color: white" data-scroll href="/Productos"  id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <span class="icon-list2"></span> Insumos
                </a>
                <div class="dropdown-menu bg-dark" aria-labelledby="dropdownMenuButton">
                    <div class="dropdown-divider" style="border-color: gold;"></div>
                    <a class="dropdown-item"style="color: white" href="/TipoInsumos"><span class="icon-hammer"></span> Tipo Insumos</a>
                    <div class="dropdown-divider" style="border-color: gold;"></div>
                    <a class="dropdown-item"style="color: white" href="/Insumos"><span class="icon-droplet"></span> Insumos</a>
                    <div class="dropdown-divider" style="border-color: gold;"></div>
                    <a class="dropdown-item" style="color: white"href="/Tarifas"><span class="icon-lastfm"></span> Tarifa Insumos</a>
                    <div class="dropdown-divider" style="border-color: gold;"></div>
                </div>
            </div>
            <div class="dropdown">
                <a class="nav-item nav-link" style="color: white" data-scroll href="/InformeClientes"><span class="icon-cogs"></span> Informes</a>
            </div>
        </div>
    </div>
@endsection
@section('content')
    <div class="container mt-5">
        <a class="btn text-white" data-toggle="modal" data-target="#modalTarifa" style="background-color: green" href="#">
            <span class="icon-plus"></span>
        </a>
        <div class="modal fade"  id="modalTarifa" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
             aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="col-sm-2"></div>
                {{-- contenido del modal --}}
                <div class="modal-content col-sm-9" style="background: black; opacity: 0.8" id='ejemplo' >
                    {{-- encabezado del modal --}}
                    <div class="modal-header text-center text-white">
                        <h4 class="modal-title w-100 font-weight-bold">{{ __('Tarifa Anual') }}</h4>
                        <a class="close" data-dismiss="modal" aria-label="Close"><span class="icon-undo2"></span></a>
                        <hr>
                    </div>
                    <div class="modal-body text-white" >
                        <form class="" method="POST" action="{{route('Tarifas.store')}}" >
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="">Insumo</label>
                                <select id="idinsumos" class="form-control" name="idinsumos" >
                                    <option value="">Seleccionar</option>
                                    @foreach($insumo as $ins)
                                        <option  value="{{$ins->id}}">{{$ins->name}}</option>
                                    @endforeach
                                </select>
                                <h6 hidden id="idinsumosErr" class="text-danger">Insumo no valido</h6>
                            </div>
                            <div class="form-group">
                                <label for="">Año</label>
                                <select id="año" class="form-control" name="año"  required onclick="añoselect()">
                                    <option value="">Seleccione</option>
                                </select>
                                <h6 hidden id="añoErr" class="text-danger">Año No valido</h6>
                            </div>
                            <div class="form-group">
                                <label for="">Valor</label>
                                <input type="text" class="form-control" id="valor" name="valor" onclick="soloNumero()" onkeypress="soloNumero()" onkeydown="soloNumero()">
                                <h6 hidden id="valorErr" class="text-danger"></h6>
                            </div>
                            <div onmousemove="ValidarTarifa()">
                                <button id="btnCrearTarifa" class="btn btn-outline-success" type="submit" onclick="registroExitoso()">Crear</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <table id="tableW" class="table-striped table-hover  prueba" >
            <thead class="text-white" style="background-color: #0a0a0a">
            <tr>
                <th scope="col">Nombre Insumo</th>
                <th scope="col">Año</th>
                <th scope="col">Valor</th>
                <th scope="col">Opciones</th>
            </tr>
            </thead>
            <tbody>
            @foreach($tarifa as $tar)
                <tr>
                    <td style="color: #0a0a0a">{{$tar->name}}</td>
                    <td style="color: #0a0a0a">{{$tar->year}}</td>
                    <td style="color: #0a0a0a">{{$tar->value}}</td>
                    <td style="color: #0a0a0a"><a class="btn btn-warning"  href="{{URL::action('ratecontroller@edit',$tar->name)}}"><span class="icon-pencil2"></span></a>
                        <button class="btn btn-danger"><span class="icon-cross"></span></button>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    <style>

    </style>
@endsection
@section('script')
    <script>
        $(document).ready(function () {
            $('#tableW').DataTable({
                language: {
                    "decimal": "",
                    "emptyTable": "No hay información",
                    "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
                    "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
                    "infoFiltered": "(Filtrado de _MAX_ total entradas)",
                    "infoPostFix": "",
                    "thousands": ",",
                    "lengthMenu": "Mostrar _MENU_ Entradas",
                    "loadingRecords": "Cargando...",
                    "processing": "Procesando...",
                    "search": "Buscar:",
                    "zeroRecords": "Sin resultados encontrados",
                    "paginate": {
                        "first": "Primero",
                        "last": "Ultimo",
                        "next": "Siguiente",
                        "previous": "Anterior"
                    }
                },
                paging: false,
                bFilter: false,
                ordering: true,
                searching: true,
                info: false,
                scrollY:        "400px",
                scrollCollapse: true,
            });
        });

        if(window.location.hash === '#Tarifa')
        {
            $('#modalTarifa').modal('show');
        }
        $('#modalTarifa').on('hide.bs.modal', function(){
            window.location.hash = '#';
        });
        $('#exampleModalLong').on('hide.bs.modal', function(){
            window.location.hash = '#';
        });
        $('#myModal').on('hide.bs.modal', function(){
            window.location.hash = '#';
        });
    </script>
@endsection

