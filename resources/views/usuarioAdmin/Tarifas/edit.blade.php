<form class="" method="POST" action="{{route('Tarifas.update',)}}" >
    {{ csrf_field() }}
    @method("PUT")
    <div class="form-group">
        <label for="">Insumo</label>
        <select id="idinsumos" class="form-control" name="idinsumos" >
            <option value="">Seleccionar</option>
            @foreach($insumo as $ins)
                <option  value="{{$ins->id}}">{{$ins->name}}</option>
            @endforeach
        </select>
    </div>
    <div class="form-group">
        <label for="">Año</label>
        <select id="año" class="form-control" name="año"  required onclick="añoselect()">
            <option value="">Seleccione</option>
        </select>
    </div>
    <div class="form-group">
        <label for="">Valor</label>
        <input type="text" class="form-control" id="valor" name="valor">
    </div>
    <div>
        <button class="btn btn-outline-success" type="submit" >Crear</button>
    </div>
</form>
