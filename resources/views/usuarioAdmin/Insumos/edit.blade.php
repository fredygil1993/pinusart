
<form class="" method="POST" action="{{route('Insumos.update',$insumo->id)}}">
    {{ csrf_field() }}
    @method("PUT")
    <div class="form-group">
        <label for="">Editar Insumo</label>
        <input type="text" name="nombre" id="nombre" class="form-control" value="{{$insumo->name}}">
    </div>
    <div class="form-group">
        <select name="idtipo" id="idtipo">
            <option value="">Seleccionar</option>
            @foreach($tipo as $tip)
                <option value="{{$tip->id}}">{{$tip->description}}</option>
            @endforeach
        </select>
    </div>
    <div>
        <button class="btn btn-outline-success" type="submit" >Editar</button>
    </div>
</form>
