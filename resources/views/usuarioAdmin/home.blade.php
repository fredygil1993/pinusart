@extends('layouts.app')
@section('name')
    <h4 style="color: green">Adminis<span style="color: orange">trador</span></h4>
@endsection
@section('info')
    <div class="collapse navbar-collapse " id="navbarNavAltMarkup">
        <div class="navbar-nav ">
            <a class="nav-item nav-link" style="color: white" data-scroll href="/home"><span class="icon-home"></span> Inicio</a>
            <div class="dropdown">
                <a class="nav-item nav-link dropdown-toggle" style="color: white" data-scroll href="/Productos"  id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <span class="icon-images"></span> Productos
                </a>
                <div class="dropdown-menu bg-dark" aria-labelledby="dropdownMenuButton">
                    <div class="dropdown-divider" style="border-color: gold;"></div>
                    <a class="dropdown-item " style="color: white"href="/CategoriaProductos"><span class="icon-cogs"></span> Categorias</a>
                    <div class="dropdown-divider" style="border-color: gold;"></div>
                    <a class="dropdown-item" style="color: white" href="/Productos"><span class="icon-images"></span> Productos</a>
                    <div class="dropdown-divider" style="border-color: gold;"></div>
                </div>
            </div>
            <div class="dropdown">
                <a class="nav-item nav-link dropdown-toggle" style="color: white" data-scroll href="/Productos"  id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <span class="icon-list2"></span> Insumos
                </a>
                <div class="dropdown-menu bg-dark" aria-labelledby="dropdownMenuButton">
                    <div class="dropdown-divider" style="border-color: gold;"></div>
                    <a class="dropdown-item"style="color: white" href="/TipoInsumos"><span class="icon-hammer"></span> Tipo Insumos</a>
                    <div class="dropdown-divider" style="border-color: gold;"></div>
                    <a class="dropdown-item"style="color: white" href="/Insumos"><span class="icon-droplet"></span> Insumos</a>
                    <div class="dropdown-divider" style="border-color: gold;"></div>
                    <a class="dropdown-item" style="color: white"href="/Tarifas"><span class="icon-lastfm"></span> Tarifa Insumos</a>
                    <div class="dropdown-divider" style="border-color: gold;"></div>
                </div>
            </div>
            <div class="dropdown">
                <a class="nav-item nav-link" style="color: white" data-scroll href="/InformeClientes"><span class="icon-cogs"></span> Informes</a>
            </div>
        </div>
    </div>
@endsection
@section('content')

    <div class="modal fade" id="modalMision" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" style="border-color: green" role="document">
            <div class="col-sm-2"></div>
    {{-- contenido del modal --}}
            <div class="modal-content col-sm-9" style="background: black; opacity: 0.8" id='ejemplo'>
                {{-- encabezado del modal --}}
                <div class="modal-header text-center text-white">
                    <h4 class="modal-title w-100 font-weight-bold">{{ __('Proposito') }}</h4>
                    <a class="close" data-dismiss="modal" aria-label="Close"><span class="icon-undo2"></span></a>
                </div>
                <form class="" method="POST" action="{{route('usuarioAdmin.store')}}">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label  class="red-text" for="">Mision</label>
                        <textarea  type="text"name="mision" id="mision" class="form-control" cols="5" rows="2"></textarea>
                        <h6 hidden id="misionErr" class="text-danger">Mision no valida</h6>
                    </div>
                    <div class="form-group">
                        <label class="red-text" for="">Vision</label>
                        <textarea  type="text"name="vision" id="vision" class="form-control" cols="5" rows="2"></textarea>
                        <h6 hidden id="visionErr" class="text-danger">Vision no valida</h6>
                    </div>
                    <div class="form-group">
                        <label class="red-text" for="">Que Hacemos</label>
                        <textarea  type="text"name="quehacemos" id="quehacemos" class="form-control" cols="5" rows="2"></textarea>
                        <h6 hidden id="quehacemosErr" class="text-danger">Que hacemos no valido</h6>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-4" onmouseover="valproposito()">
                            <button class="btn text-black btn-sm btn-outline-success" type="submit" id="buttonRegistrar" name="buttonRegistrar" onclick="cambiosExitosos()">Crear Proposito</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="wrap mt-5">
        <div class="tarjeta-wrap">
            <div class="tarjeta">
                <div class="adelante card1">
                    <h1 class="mt-5 p-lg-5" align="center">Mision</h1>
                </div>
                <div class="atras">
                    @foreach($Proposito as $pro)
                        <p class="card-text">{{$pro->mision}}</p>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="tarjeta-wrap">
            <div class="tarjeta">
                <div class="adelante card2">

                    <h1 class="mt-5 p-lg-5" align="center">Vision</h1>

                </div>
                <div class="atras">
                    @foreach($Proposito as $pro)
                        <p class="card-text">{{$pro->vision}}</p>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="tarjeta-wrap">
            <div class="tarjeta">
                <div class="adelante card3">
                    <h1 class="mt-5" align="center">¿Que</h1>
                    <h1 class="" align="center">Hacemos?</h1>
                </div>
                <div class="atras">
                    @foreach($Proposito as $pro)
                        <p class="card-text">{{$pro->que_hacemos}}</p>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    <div class="container" align="center">
        <a class="btn btn-success" data-toggle="modal" style="background-color: #3a800d"
           data-target="#modalMision"  href="#">Crear Proposito
        </a>
        {{--  {{URL::action('PurposesController@update',$pro->id)}} --}}
    </div>
    <style>
        body{
            background-image: url("{{asset('image/fondo2.jpg')}}");
            Background-size:100%;
            Background-repeat:no-repeat;
            background: -webkit-gradient(linear, left top, right top, from(rgba(255, 153, 0, 0.81)), to(rgba(255, 153, 0, 0.81))), url("../image/fondo.jpg");
            background: -webkit-linear-gradient(left, rgba(255, 153, 0, 0.81), rgba(255, 153, 0, 0.81)), url("../image/fondo.jpg");
            background: -o-linear-gradient(left, rgba(255, 153, 0, 0.81), rgba(255, 153, 0, 0.81)), url("../image/fondo.jpg");
            background: linear-gradient(to right,rgba(255, 153, 0, 0.81), rgba(255, 153, 0, 0.81)), url("../image/fondo.jpg");
            background-size: cover;
        }
        *{
            margin: 0;
            padding: 0;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
        }
        .red-text{
            color: white;
        }
        .wrap{
            width: 1100px;
            margin: 50px auto;
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-pack: center;
            -webkit-justify-content: center;
            -ms-flex-pack: center;
            justify-content: center;
            border-radius: 10px 10px 10px 10px;
            -moz-border-radius: 10px 10px 10px 10px;
            -webkit-border-radius: 10px 10px 10px 10px;
            border: 0px solid #000000;
        }
        .tarjeta-wrap{
            margin: 10px;
            -webkit-perspective: 800;
            perspective: 800;
        }
        .tarjeta{
            width: 300px;
            height: 350px;
            background: #0b2e13;
            position: relative;
            -webkit-transform-style: preserve-3d;
            transform-style: preserve-3d;
            -webkit-transition: .7s ease;
            transition: .7s ease;
            -webkit-box-shadow: 0px 10px 15px -5px rgba(0,0,0,0.65);
            box-shadow: 0px 10px 15px -5px rgba(0,0,0,0.65);
        }
        .adelante, .atras{
            width: 100%;
            height: 100%;
            position: absolute;
            top: 0;
            left: 0;
            -webkit-backface-visibility: hidden;
            backface-visibility: hidden;
        }
        .adelante{
            width: 100%;
            color: white;
        }

        .atras{
            -webkit-transform: rotateY(180deg);
            transform: rotateY(180deg);

            padding: 15px;
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-pack: center;
            -webkit-justify-content: center;
            -ms-flex-pack: center;
            justify-content: center;
            -webkit-box-align: center;
            -webkit-align-items: center;
            -ms-flex-align: center;
            align-items: center;

            text-align: center;
            color: white;
            font-family: "open sans";
            background: #707772;
        }

        .tarjeta-wrap:hover .tarjeta{
            -webkit-transform: rotateY(180deg);
            transform: rotateY(180deg);
        }

        .card1{
            background-color: #3a800d;
            background-size: cover;
        }
        .card2{
            background-color: #3a800d;
            background-size: cover;
        }
        .card3{
            background-color: #3a800d;
            background-size: cover;
        }
    </style>
@endsection
@section('script')
    <script>
        if (window.location.hash === '#Mision') {
            $('#modalMision').modal('show');
        }
        $('#modalMision').on('hide.bs.modal', function () {
            window.location.hash = '#';
        });
        $('#exampleModalLong').on('hide.bs.modal', function () {
            window.location.hash = '#';
        });
        $('#myModal').on('hide.bs.modal', function () {
            window.location.hash = '#';
        });

    </script>
@endsection
