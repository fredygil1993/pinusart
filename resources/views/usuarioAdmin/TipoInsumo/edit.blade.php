<form class="" method="POST" action="{{route('TipoInsumos.update', $tipoinsumo->id)}}">
    {{ csrf_field() }}
    @method("PUT")
    <div class="form-group">
        <label for="">Editar tipo Insumo</label>
        <input type="text" name="description" id="description" class="form-control" value="{{$tipoinsumo->description}}">
    </div>
    <div>
        <button class="btn btn-outline-success" type="submit" >Editar</button>
    </div>
</form>
