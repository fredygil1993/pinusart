@extends('layouts.app')
@section('name')
    <h4 style="color: green">Adminis<span style="color: orange">trador</span></h4>
@endsection
@section('info')
    <div class="collapse navbar-collapse " id="navbarNavAltMarkup">
        <div class="navbar-nav ">
            <a class="nav-item nav-link" style="color: white" data-scroll href="/home"><span class="icon-home"></span> Inicio</a>
            <div class="dropdown">
                <a class="nav-item nav-link dropdown-toggle" style="color: white" data-scroll href="/Productos"  id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <span class="icon-images"></span> Productos
                </a>
                <div class="dropdown-menu bg-dark" aria-labelledby="dropdownMenuButton">
                    <div class="dropdown-divider" style="border-color: gold;"></div>
                    <a class="dropdown-item " style="color: white"href="/CategoriaProductos"><span class="icon-cogs"></span> Categorias</a>
                    <div class="dropdown-divider" style="border-color: gold;"></div>
                    <a class="dropdown-item" style="color: white" href="/Productos"><span class="icon-images"></span> Productos</a>
                    <div class="dropdown-divider" style="border-color: gold;"></div>
                </div>
            </div>
            <div class="dropdown">
                <a class="nav-item nav-link dropdown-toggle" style="color: white" data-scroll href="/Productos"  id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <span class="icon-list2"></span> Insumos
                </a>
                <div class="dropdown-menu bg-dark" aria-labelledby="dropdownMenuButton">
                    <div class="dropdown-divider" style="border-color: gold;"></div>
                    <a class="dropdown-item"style="color: white" href="/TipoInsumos"><span class="icon-hammer"></span> Tipo Insumos</a>
                    <div class="dropdown-divider" style="border-color: gold;"></div>
                    <a class="dropdown-item"style="color: white" href="/Insumos"><span class="icon-droplet"></span> Insumos</a>
                    <div class="dropdown-divider" style="border-color: gold;"></div>
                    <a class="dropdown-item" style="color: white"href="/Tarifas"><span class="icon-lastfm"></span> Tarifa Insumos</a>
                    <div class="dropdown-divider" style="border-color: gold;"></div>
                </div>
            </div>
            <div class="dropdown">
                <a class="nav-item nav-link" style="color: white" data-scroll href=""><span class="icon-cogs"></span> Informes</a>
            </div>
        </div>
    </div>
@endsection
@section('content')
    <div class=" container" style="margin-top: 10%">
        <table id="tablep" class="table-striped table-hover"  >
            <thead class="text-white" style="background-color: #0a0a0a">
            <tr>
                <th scope="col">Id Cliente</th>
                <th scope="col">Nombre cliente</th>
                <th scope="col">Rol</th>
                <th scope="col">Informe</th>
            </tr>
            </thead>
            <tbody>
            @foreach($cliente as $fic)
                <tr>
                    <td style="color: #0a0a0a">{{$fic->id}}</td>
                    <td style="color: #0a0a0a">{{$fic->name}}</td>
                    <td style="color: #0a0a0a">{{$fic->type_rol}}</td>
                    <td style="color: #0a0a0a"><a class="btn btn-outline-dark text-white" href="/Informes/{{$fic->id}}" style="margin-left: 18%; "><span class="icon-eye" style="color: #0a0a0a"></span></a></td>
                </tr>
            @endforeach

            </tbody>
        </table>
    </div>
@endsection
@section('script')
    <script>
        $(document).ready(function () {
            $('#tablep').DataTable({
                language: {
                    "decimal": "",
                    "emptyTable": "No hay información",
                    "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
                    "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
                    "infoFiltered": "(Filtrado de _MAX_ total entradas)",
                    "infoPostFix": "",
                    "thousands": ",",
                    "lengthMenu": "Mostrar _MENU_ Entradas",
                    "loadingRecords": "Cargando...",
                    "processing": "Procesando...",
                    "search": "Buscar:",
                    "zeroRecords": "Sin resultados encontrados",
                    "paginate": {
                        "first": "Primero",
                        "last": "Ultimo",
                        "next": "Siguiente",
                        "previous": "Anterior"
                    }
                },
                paging:false,
                bFilter:false,
                ordering:true,
                searching:true,
                info:false,
            });
        });
    </script>
@endsection
