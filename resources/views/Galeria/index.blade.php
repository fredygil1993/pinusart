@extends('layouts.app')
@section('name')
    <a href="/"><h4 style="color: green; font-size: 16px">Pinus<span
                style="color: orange">Art</span></h4></a>
    <b><a href="#quehacemos" style="font-size: 16px " class="mx-2"><span style="color: green">¿Que</span><span style="color: orange">Hacemos?</span></a></b>
    <b><a href="#contactanos" style="font-size: 16px" class="mx-2" ><span style="color: green">Contac</span><span style="color: orange">tanos...<span class="icon-phone"></span></span></a></b>
    <b><a href="/Galeria"  class="mx-2" style="font-size: 16px; " ><span style="color: green">Gale</span><span style="color: orange">ria<span class="icon-images mx-1"></span></span></a></b>
@endsection
@section('content')
        <div class="mt-5">
            <ul class="galeria">
                @foreach($galeria as $galery )
                    <li class="galeria__item"><img src="GaleriaProductos/{{$galery->photo}}" style="width: 80%; height: 80%" class="galeria__img"></li>
                @endforeach

            </ul>
        </div>
    <style>
        * {
            box-sizing: border-box;
        }
        body {
            margin: 0;
            background: #111;
        }

        img {
            display: block;
            max-width: 100%;
        }

        .galeria {
            padding: 20px;
            display: flex;
            flex-wrap: wrap;
            justify-content: center;
        }

        .galeria__item {
            width: 80%;
            cursor: pointer;
        }
        @media (min-width:480px) {
            .galeria__item {
                width: 48%;
                margin: 5px;
            }
        }
        @media (min-width:768px) {
            .galeria__item {
                width: 30%;
            }
        }
        @media (min-width:1024px) {
            .galeria__item {
                width: 20%;
                margin: 15px;
            }
        }

        .modal {
            position: fixed;
            width: 100%;
            height: 100vh;
            background: rgba(0,0,0,0.7);
            top: 0;
            left: 0;

            display: flex;
            justify-content: center;
            align-items: center;
        }
        .modal__img {
            width: 40%;
            max-width: 700px;
        }

        .modal__boton {
            width: 50px;
            height: 50px;
            color: #fff;
            font-weight: bold;
            font-size: 25px;
            font-family: monospace;
            line-height: 50px;
            text-align: center;
            background: red;
            border-radius: 50%;
            cursor: pointer;

            position: absolute;
            right: 10px;
            top: 10px;
        }
    </style>
    <script>

        $('.galeria__img').click(function(e){
        var img = e.target.src;
        var modal = '<div class="modal" id="modal"><img src="'+ img + '" class="modal__img"><div class="modal__boton" id="modal__boton">X</div></div>';
        $('body').append(modal);
        $('#modal__boton').click(function(){
        $('#modal').remove();
        })
        });


        $(document).keyup(function(e){
        if (e.which==27) {
        $('#modal').remove();
        }
        })
    </script>
@endsection
