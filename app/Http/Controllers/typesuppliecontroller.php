<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\typesupplie;

class typesuppliecontroller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tipoinsumo = typesupplie::all();
        return view('usuarioAdmin.TipoInsumo.index',compact('tipoinsumo'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $tipoinsumo = new typesupplie();
        $tipoinsumo->description = $request->input('descripcion');
        $tipoinsumo->save();

        return redirect()->route('TipoInsumos.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tipoinsumo = typesupplie::find($id);
       return view('usuarioAdmin.TipoInsumo.edit',compact('tipoinsumo'));
    }

    /**resources/views/usuarioAdmin/TipoInsumo/edit.blade.php
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $tipoinsumo = typesupplie::find($id);
        $tipoinsumo->description = $request->description;
        $tipoinsumo->save();

        return redirect()->route('TipoInsumos.index');
    }

    /**resources/views/usuarioAdmin/TipoInsumo/index.blade.php
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
