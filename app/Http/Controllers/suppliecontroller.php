<?php

namespace App\Http\Controllers;

use App\Model\typesupplie;
use Illuminate\Http\Request;
use App\Model\supplie;

class suppliecontroller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tipo = typesupplie::all();
        $insumo = supplie::select('supplies.id','typesupplies.description','supplies.name')
            ->join('typesupplies', 'typesupplies.id', '=', 'supplies.idtypesupplie')
            ->get();
        return view('usuarioAdmin.Insumos.index', compact('insumo','tipo'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $insumo = new supplie();
        $insumo->idtypesupplie = $request->input('idtipo');
        $insumo->name = $request->input('nombre');
        $insumo->save();

        return redirect()->route('Insumos.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tipo = typesupplie::all();
        $insumo = supplie::find($id);
        return view('usuarioAdmin.Insumos.edit',compact('insumo','tipo'));
    }

    /**$insumo = supplie::find($id);
    return view('usuarioAdmin.Insumos.edit',compact('insumo'));
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $insumo = supplie::find($id);
        $insumo->name = $request->nombre;
        $insumo->idtypesupplie = $request->idtipo;
        $insumo->save();
        return redirect()->route('Insumos.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
