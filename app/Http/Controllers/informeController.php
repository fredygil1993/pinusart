<?php

namespace App\Http\Controllers;

use App\Model\datasheets;
use App\Model\productsfinal;
use Illuminate\Http\Request;


class informeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cliente = productsfinal::select('clients.id','clients.name','rol.type_rol')
            ->join('products','products.id','=','productsfinals.idproduct')
            ->join('datasheets','datasheets.id','=','productsfinals.iddatasheet')
            ->join('states','states.id','=','datasheets.idstate')
            ->join('clients','datasheets.idclient','=','clients.id')
            ->join('users','users.id','=','clients.id_user')
            ->join('rol','rol.id','=','users.role_id')
            ->groupBy('clients.id')
            ->get();
        return view('usuarioAdmin.Informes.InformeClientes.index',compact('cliente') );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
