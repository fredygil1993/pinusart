<?php

namespace App\Http\Controllers;

use App\Model\datasheets;
use App\Model\productsfinal;
use App\Model\Products;
use App\Model\clients;
use App\Model\annualrate;
use App\Model\state;
use App\Model\supplie;
use App\Model\typesupplie;
use Illuminate\Http\Request;
use function Sodium\add;


class ProductFinalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $productos = Products::all();
        $usuario = \auth::user();
        $usuario2 = \auth::user()->id;
        $clientes = clients::all();
        $estado = state::select('id')
            ->where('id','=','1')
            ->get();
        $cliente = clients::select('id')
        ->where('id','=', $usuario2)
        ->get();

        $ficha = datasheets::select('id')
            ->where('id', '=', $usuario2)
            ->orderBy('created_at', 'desc')->take(1)
            ->get();

        $tarifa2 = annualrate::select('supplies.name', 'annualrates.id', 'annualrates.year', 'annualrates.value', 'supplies.idtypesupplie', 'typesupplies.description')
            ->join('supplies', 'supplies.id', '=', 'annualrates.idsupplie')
            ->join('typesupplies', 'supplies.idtypesupplie', '=', 'typesupplies.id')
            ->where('idtypesupplie','=','1')
            ->get();

        $tarifa = annualrate::select('supplies.name', 'annualrates.id', 'annualrates.year', 'annualrates.value', 'supplies.idtypesupplie', 'typesupplies.description')
            ->join('supplies', 'supplies.id', '=', 'annualrates.idsupplie')
            ->join('typesupplies', 'supplies.idtypesupplie', '=', 'typesupplies.id')
            ->where('idtypesupplie','=','2')
            ->get();
        return view('usuarioStandar.FichaTecnica.ProductoFinal.index', compact('usuario','cliente', 'clientes', 'productos', 'tarifa', 'usuario2', 'ficha','tarifa2', 'estado'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $Ficha = new datasheets();
        $Ficha->idclient	 = $request->input('idcliente');
        $Ficha->idstate 	 = $request->input('idestado');
        $Ficha->date		 = $request->input('fechaActual');
        $Ficha->quantity     = $request->input('cantidad');
        $Ficha->total		 = $request->input('total');
        $Ficha->save();

        $final = new productsfinal();
        $final->iddatasheet        = $Ficha->id;
        $final->idproduct           = $request->input('idproducto');
        $final->idrate              = $request->input('idtarifa');
        $final->description         = $request->input('descripcion');
        $final->save();

        $final2 = new productsfinal();
        $final2->iddatasheet         = $Ficha->id;
        $final2->idproduct           = $request->input('idproducto');
        $final2->idrate              = $request->input('idrate2');;
        $final2->description         = $request->input('descripcion');
        $final2->save();

        return redirect()->route('Pedido.index');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $informesito = \DB::table('productsfinals')
        ->select(\DB::raw('productsfinals.idproduct, COUNT(*) as count_row'))
        ->groupBy(\DB::raw('productsfinals.idproduct'))
        ->get();

        return view('',compact('informesito'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $usuario = \auth::user();
        $productos = Products::all();
        $clientes = clients::all();
        $usuario2 = \auth::user()->id;
        $estado = state::select('id')
            ->where('id','=','1')
            ->get();
        $cliente = clients::select('id')
            ->where('id','=', $usuario2)
            ->get();
        $data = datasheets::find($id);
        $producto=Products::find($data->id);
        $tarifa2 = annualrate::select('supplies.name', 'annualrates.id', 'annualrates.year', 'annualrates.value', 'supplies.idtypesupplie', 'typesupplies.description')
            ->join('supplies', 'supplies.id', '=', 'annualrates.idsupplie')
            ->join('typesupplies', 'supplies.idtypesupplie', '=', 'typesupplies.id')
            ->where('idtypesupplie','=','1')
            ->get();
        $insumo = productsfinal::select('supplies.name', 'annualrates.id', 'annualrates.year', 'annualrates.value', 'supplies.idtypesupplie', 'typesupplies.description')
            ->join('annualrates', 'annualrates.id', '=','productsfinals.idrate')
            ->join('supplies', 'supplies.id', '=', 'annualrates.idsupplie')
            ->join('typesupplies', 'supplies.idtypesupplie', '=', 'typesupplies.id')
            ->where('idtypesupplie','=','1','AND','productsfinals.iddatasheet','=',$id)
            ->first();
        $insumo2 = productsfinal::select('supplies.name', 'annualrates.id', 'annualrates.year', 'annualrates.value', 'supplies.idtypesupplie', 'typesupplies.description')
            ->join('annualrates', 'annualrates.id', '=','productsfinals.idrate')
            ->join('supplies', 'supplies.id', '=', 'annualrates.idsupplie')
            ->join('typesupplies', 'supplies.idtypesupplie', '=', 'typesupplies.id')
            ->where('idtypesupplie','=','2','AND','productsfinals.iddatasheet','=',$id)
            ->first();
        $tarifa = annualrate::select('supplies.name', 'annualrates.id', 'annualrates.year', 'annualrates.value', 'supplies.idtypesupplie', 'typesupplies.description')
            ->join('supplies', 'supplies.id', '=', 'annualrates.idsupplie')
            ->join('typesupplies', 'supplies.idtypesupplie', '=', 'typesupplies.id')
            ->where('idtypesupplie','=','2')
            ->get();


        return view('usuarioStandar.Pedido.edit',compact('productos','estado','cliente','tarifa2','tarifa','clientes','usuario','usuario2','insumo2','data','producto','insumo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $Ficha = datasheets::find($id);

        $Ficha->idclient	 = $request->input('idcliente');
        $Ficha->idstate 	 = $request->input('idestado');
        $Ficha->date		 = $request->input('fechaActual');
        $Ficha->quantity     = $request->input('cantidad');
        $Ficha->total		 = $request->input('total');
        $Ficha->save();

        $final = productsfinal::find($id);
        $final->iddatasheet        = $Ficha->id;
        $final->idproduct           = $request->input('idproducto');
        $final->idrate              = $request->input('idtarifa');
        $final->description         = $request->input('descripcion');
        $final->save();

        $final2 = productsfinal::find($id);
        $final2->iddatasheet         = $Ficha->id;
        $final2->idproduct           = $request->input('idproducto');
        $final2->idrate              = $request->input('idrate2');;
        $final2->description         = $request->input('descripcion');
        $final2->save();

        return redirect()->route('Pedido.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
