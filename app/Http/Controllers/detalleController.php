<?php

namespace App\Http\Controllers;

use App\Model\productsfinal;
use Illuminate\Http\Request;
use App\Model\clients;

class detalleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view('usuarioAdmin.Informes.InformeClientes.informe',compact('cliente'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $cliente = productsfinal::select('datasheets.quantity','datasheets.id','datasheets.date','products.name_product','supplies.name','typesupplies.description','states.state')
            ->join('products','products.id','=','productsfinals.idproduct')
            ->join('annualrates','annualrates.id','=','productsfinals.idrate')
            ->join('supplies','supplies.id','=','annualrates.idsupplie')
            ->join('typesupplies','typesupplies.id','=','supplies.idtypesupplie')
            ->join('datasheets','productsfinals.iddatasheet','=','datasheets.id')
            ->join('states','states.id','=','datasheets.idstate')
            ->where('datasheets.idclient','=',$id)
            ->get();

        return view('usuarioAdmin.Informes.InformeClientes.informe', compact('cliente'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
