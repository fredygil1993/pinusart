<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Products;
use App\Model\CategoryProducts;


class  ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()

    {
        $categorias = CategoryProducts::all();
        $usuario = \Auth::user();
        $productos =  Products::select('products.id','category_products.nameCategory','products.photo','products.name_product','products.size','products.high','products.width','products.depth','products.year', 'products.value_unit','products.value_max12','products.description','products.state')
            ->join('category_products', 'category_products.id', '=', 'products.type_product')
            ->get();
        return view('usuarioAdmin.Productos.index', compact('productos','categorias','usuario'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('usuarioAdmin.Productos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->hasFile('imagen')){
            $file = $request->file('imagen');
            $name = time().$file->getClientOriginalName();
            $file->move(public_path().'/GaleriaProductos/', $name);
        }

        $request->validate([
            'alto'=>'required'
        ]);
        $producto = new Products();
        $producto->photo = $name;
        $producto->id_products = $request->input('id_products');
        $producto->type_product = $request->input('categoria');
        $producto->name_product = $request->input('nombre');
        $producto->size         = $request->input('tamaño');
        $producto->high    =      $request->input('alto');
        $producto->width =        $request->input('ancho');
        $producto->year =         $request->input('año');
        $producto->value_unit =   $request->input('precio');
        $producto->value_max12 =  $request->input('precio2');
        $producto->description =  $request->input('descripcion');
        $producto->save();

        return redirect()->route('Productos.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categorias = CategoryProducts::all();
        $producto = Products::find($id);
        return view('usuarioAdmin.Productos.edit',compact('producto','categorias'));
    }

    /**resources/views/usuarioAdmin/Productos/edit.blade.php
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $producto = Products::find($id);
        $producto->id_products =  $request->id_products;
        $producto->type_product = $request->type_product;
        $producto->name_product = $request->name_product;
        $producto->size         = $request->size;
        $producto->high    =      $request->high;
        $producto->width =        $request->width;
        $producto->depth =        $request->depth;
        $producto->year =         $request->year;
        $producto->value_unit =   $request->value_unit;
        $producto->value_max12 =  $request->value_max12;
        $producto->description =  $request->description;
        $producto->state       =  $request->state;
        $producto->save();
        return redirect()->route('Productos.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
