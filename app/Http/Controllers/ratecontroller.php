<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\annualrate;
use App\Model\supplie;

class ratecontroller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $insumo = supplie::all();
        $tarifa = annualrate::select('supplies.name','annualrates.year','annualrates.value')
            ->join('supplies', 'supplies.id', '=', 'annualrates.idsupplie')
            ->get();

        return view('usuarioAdmin.Tarifas.index', compact('tarifa', 'insumo'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $tarifa = new annualrate();
        $tarifa->idsupplie = $request->input('idinsumos');
        $tarifa->year = $request->input('año');
        $tarifa->value = $request->input('valor');
        $tarifa->save();

        return redirect()->route('Tarifas.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tarifa = annualrate::find($id);
        dd($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $tarifa = annualrate::find($id);
        $tarifa->idsupplie = $request->idsupplie;
        $tarifa->year = $request->year;
        $tarifa->value = $request->value;
        $tarifa->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
