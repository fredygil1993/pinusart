<?php

namespace App\Http\Controllers;

use App\Model\Category;
use App\Model\CategoryProducts;
use Illuminate\Http\Request;


class ProductCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()

    {
        $Categories = CategoryProducts::all();
        return view('usuarioAdmin.CategoriaProductos.index',compact('Categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('usuarioAdmin.CategoriaProductos.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $Categories = new CategoryProducts();
        $Categories->nameCategory = $request->input('categoria');
        $Categories->save();

        return redirect()->route('CategoriaProductos.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $Categories = CategoryProducts::find($id);
        return view('usuarioAdmin.CategoriaProductos.edit',compact('Categories'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request return redirect()->route('CategoriaProductos.index');
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $Categories = CategoryProducts::find($id);
        $Categories->nameCategory = $request->nameCategory;
        $Categories->save();
        return redirect()->route('CategoriaProductos.index');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
