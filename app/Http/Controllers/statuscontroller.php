<?php

namespace App\Http\Controllers;

use App\Model\clients;
use App\Model\productsfinal;
use Illuminate\Http\Request;
use App\Model\datasheets;
use App\Model\Products;
use App\Model\annualrate;
use App\Model\state;
use App\Model\typesupplie;

class statuscontroller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Nombre producto</thNombre producto</thHttp\Response
     */
    public function index()
    {
        $usuario = \auth::user();
        $usuario2 = \auth::user()->id;
        $clientes = clients::all();
        $ficha = productsfinal::select('products.id','products.name_product','datasheets.date','datasheets.quantity','datasheets.total','states.state')
            ->join('products','products.id','=','productsfinals.idproduct')
            ->join('datasheets','datasheets.id','=','productsfinals.iddatasheet')
            ->join('clients','clients.id','=','datasheets.idclient')
            ->join('states','states.id','=','datasheets.idstate')
            ->groupBy('products.id','products.name_product','datasheets.date','datasheets.quantity','datasheets.total','states.state')
            ->where('clients.id','=', $usuario2)
            ->get();
        return view('usuarioStandar.Pedido.index',compact('clientes','usuario2','usuario','ficha'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
