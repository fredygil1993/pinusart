<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\clients;
use App\Model\purposes;

class homeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }




    public function index()
    {
        $clientes = clients::all();
        $Proposito = purposes::all();
        return view('usuarioAdmin.home', compact('Proposito','clientes'));
    }

    public function standar()
    {
        $usuario=\auth::user();
        $clientes =clients::all();

        foreach ($clientes as $cliente) {
            if(auth()->check() && $cliente->id_user == $usuario->id) {
                return view('usuarioStandar.Cliente.index',compact('clientes','usuario'));
            }
        }

        return view('usuarioStandar.index');
    }
}
