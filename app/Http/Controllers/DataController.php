<?php

namespace App\Http\Controllers;

use App\Model\productsfinal;
use App\Model\state;
use Illuminate\Http\Request;
use App\Model\datasheets;
use App\Model\clients;


class DataController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $usuario=\auth::user();
        $usuario2=\auth::user()->id;
        $clientes = clients::all();
        return view('usuarioStandar.FichaTecnica.index', compact('clientes','usuario','ficha','usuario2','estado'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $Ficha = new datasheets();
        $Ficha->idclient	 = $request->input('idcliente');
        $Ficha->idstate 	 = $request->input('idestado');
        $Ficha->date		 = $request->input('fechaActual');
        $Ficha->quantity     = $request->input('cantidad');
        $Ficha->total		 = $request->input('total');
        $Ficha->save();

        return redirect()->route('ProductoFinal.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
