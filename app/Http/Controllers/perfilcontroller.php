<?php

namespace App\Http\Controllers;

use App\Model\clients;
use Illuminate\Http\Request;
use PhpParser\Node\Stmt\DeclareDeclare;

class perfilcontroller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $usuario = \auth::user();

        $cliente = clients::select('users.email','clients.name','clients.lastname','clients.phone','clients.address','clients.avatar')
            ->join('users','users.id','=','clients.id_user')
            ->where('clients.id_user', '=', $usuario->id)
            ->get();

        return view('usuarioStandar.Perfil.index', compact('cliente','usuario'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $usuario = \auth::user();
        $usuario2 = \auth::user()->id;
        $clientes = clients::all();
        $clientes = clients::find($id);
        $usuario3 = clients::select('users.email')
            ->join('users','users.id','=','clients.id_user')
            ->where('clients.id_user', '=', $usuario2)
            ->get();

        return view('usuarioStandar.Perfil.edit',compact('clientes','usuario','usuario2','usuario3'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $cliente = clients::find($id);
        /**$cliente->avatar = $request->avatar;
        $cliente->id_user = $request->id_user;*/
        $cliente->name = $request->name;
        $cliente->lastname = $request->lastname;
        $cliente->phone = $request->phone;
        $cliente->address = $request->address;
        $cliente->save();
        return redirect()->route('Perfil.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
