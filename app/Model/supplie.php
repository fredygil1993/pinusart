<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class supplie extends Model
{
    protected $fillable = [
        'idtypesupplie','name','state'
    ];
}
