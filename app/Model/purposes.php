<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class purposes extends Model
{
    protected $table = 'purposes';
    protected  $fillable =[
        'mision','vision','que_hacemos'
    ];
}
