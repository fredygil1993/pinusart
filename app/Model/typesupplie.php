<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class typesupplie extends Model
{
    protected $fillable = [
        'description'
    ];
}
