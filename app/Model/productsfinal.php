<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class productsfinal extends Model
{
    protected $fillable = [
        'iddatasheet','idproduct','idrate','description'
    ];
}
