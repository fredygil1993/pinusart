<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    protected $fillable =[
        'id_products',
        'photo',
        'type_product',
        'name_product',
        'size',
        'high','width','depth',
        'year',
        'value_unit',
        'value_max12',
        'description',
        'state'
    ];
}
