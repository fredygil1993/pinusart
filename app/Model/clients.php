<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class clients extends Model
{
    protected $fillable = [
        'id_user','avatar','name', 'lastname','phone','address'
    ];

    public function tipe(){
        return $this->belongsTo('App\User','id');
    }

}
