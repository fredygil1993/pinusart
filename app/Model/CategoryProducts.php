<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class CategoryProducts extends Model
{
    protected $fillable=[
        'nameCategory'
    ];
}
