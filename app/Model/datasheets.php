<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class datasheets extends Model
{
    protected $fillable =[
        'idclient','idstate','date','quantity','total'
    ];
}
