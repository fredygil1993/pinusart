<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class annualrate extends Model
{
   protected $fillable =[
       'idsupplie','year','value'
   ];
}
